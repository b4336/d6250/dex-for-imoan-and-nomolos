// const { ethers } = require("hardhat");

async function main() {
  console.log(`Preparing Deployment... \n`)

  // 1. Fetch contracts to deploy
  const ERC20DecEx = await ethers.getContractFactory("ERC20DecEx")
  const ERC20Token = await ethers.getContractFactory("ERC20Token")

  // 2. Fetch accounts; the 20 (0th-19th) accounts provided by Hardhat, or 2 accounts for Goerli
  const accounts = await ethers.getSigners()
  console.log("Accounts Fetched: ")
  for(let i = 0; i<=accounts.length-1; i++) {
    if (i<=9) {
      console.log(` ${i}-th: ${accounts[i].address}`) // Can verify with the first 10 accounts shown on the Hardhat node
    } else {
      console.log(`${i}-th: ${accounts[i].address}`)  // Can verify with the last 10 accounts shown on the Hardhat node
    }
  }
  
  // 3. Deploy contracts; can verify that these contracts are NOT deployed to any of the Hardhat accounts
  //    accounts[1] is the feeAccount of the DecEx, rather than the DecEx itself; accounts[0] reserved for deployer
  const erc20DecEx = await ERC20DecEx.deploy(accounts[1].address, 1)   // Passes in feeAccount and feePercent
  const erc20TokenYAK = await ERC20Token.deploy("Yak", "YAK", 18, 11)  // YAK has only 11 coins in total
  const erc20TokenIMA = await ERC20Token.deploy("Imoan", "IMA", 18, 1000000)
  const erc20TokenNML = await ERC20Token.deploy("Nomolos", "NML", 18, 1000000)

  await erc20DecEx.deployed()
  await erc20TokenYAK.deployed()
  await erc20TokenIMA.deployed()
  await erc20TokenNML.deployed()

  console.log(`\nThe ERC-20 Decentralized Exchange Itself and Its Fee Account Respectively Deployed to: \n ${erc20DecEx.address} \n ${await erc20DecEx.feeAccount()} \n`)
  console.log(`Token YAK Deployed to: ${erc20TokenYAK.address}`)
  console.log(`Token IMA Deployed to: ${erc20TokenIMA.address}`)
  console.log(`Token NML Deployed to: ${erc20TokenNML.address}`)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
