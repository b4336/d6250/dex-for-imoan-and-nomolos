const { ethers } = require("hardhat")

const config = require("../src/config.json")

const convertToWei = (n) => {
    return ethers.utils.parseUnits(n.toString(), "ether")
}

const wait = (seconds) => {
    const milliseconds = seconds * 1000
    return new Promise(resolve => setTimeout(resolve, milliseconds))
}

async function main() {
    // Fetch Accounts from Wallet (Unlocked)
    const accounts = await ethers.getSigners()

    // Fetch the Network
    const { chainId } = await ethers.provider.getNetwork()
    console.log("Using chainId: ", chainId)
    
    // Set up Accounts; accounts[1] should have been assigned to the DecEx fee account during deployment
    const deployer = accounts[0]
    const user1 = accounts[2]
    const user2 = accounts[3]
    const user3 = accounts[4]

    // Fetch Deployed Tokens (YAK, IMA & NML)
    const ERC20TokenYAK = await ethers.getContractAt("ERC20Token", config[chainId].ERC20TokenYAK.address)
    console.log(`YAK Fetched at ${ERC20TokenYAK.address}\n`)

    const ERC20TokenIMA = await ethers.getContractAt("ERC20Token", config[chainId].ERC20TokenIMA.address)
    console.log(`IMA Fetched at ${ERC20TokenIMA.address}\n`)

    const ERC20TokenNML = await ethers.getContractAt("ERC20Token", config[chainId].ERC20TokenNML.address)
    console.log(`NML Fetched at ${ERC20TokenNML.address}\n`)
    
    // Fetch the Deployed Decentralized Exchange
    const ERC20DecEx = await ethers.getContractAt("ERC20DecEx", config[chainId].ERC20DecEx.address)
    console.log(`The Dex Fetched at ${ERC20DecEx.address}\n`)

    let amount1, amount2
    let transaction, result

    // 0. Distribute Tokens (YAK, IMA & NML) from Deployer to Users (so that the users can start depositing)
    amount1 = convertToWei(2)
    amount2 = convertToWei(250000)

    // 2 YAK to User 1
    transaction = await ERC20TokenYAK.connect(deployer).transfer(user1.address, amount1)
    await transaction.wait()
    console.log(`Transferred ${amount1} of YAK \n from ${deployer.address} \n to ${user1.address}\n`)
    // 2 YAK to User 2
    transaction = await ERC20TokenYAK.connect(deployer).transfer(user2.address, amount1)
    await transaction.wait()
    console.log(`Transferred ${amount1} of YAK \n from ${deployer.address} \n to ${user2.address}\n`)
    // 2 YAK to User 3
    transaction = await ERC20TokenYAK.connect(deployer).transfer(user3.address, amount1)
    await transaction.wait()
    console.log(`Transferred ${amount1} of YAK \n from ${deployer.address} \n to ${user3.address}\n`)

    // 250000 IMA to User 1
    transaction = await ERC20TokenIMA.connect(deployer).transfer(user1.address, amount2)
    await transaction.wait()
    console.log(`Transferred ${amount2} of IMA \n from ${deployer.address} \n to ${user1.address}\n`)
    // 250000 IMA to User 2
    transaction = await ERC20TokenIMA.connect(deployer).transfer(user2.address, amount2)
    await transaction.wait()
    console.log(`Transferred ${amount2} of IMA \n from ${deployer.address} \n to ${user2.address}\n`)
    // 250000 IMA to User 3
    transaction = await ERC20TokenIMA.connect(deployer).transfer(user3.address, amount2)
    await transaction.wait()
    console.log(`Transferred ${amount2} of IMA \n from ${deployer.address} \n to ${user3.address}\n`)

    // 250000 NML to User 1
    transaction = await ERC20TokenNML.connect(deployer).transfer(user1.address, amount2)
    await transaction.wait()
    console.log(`Transferred ${amount2} of NML \n from ${deployer.address} \n to ${user1.address}\n`)
    // 250000 NML to User 2
    transaction = await ERC20TokenNML.connect(deployer).transfer(user2.address, amount2)
    await transaction.wait()
    console.log(`Transferred ${amount2} of NML \n from ${deployer.address} \n to ${user2.address}\n`)
    // 250000 NML to User 3
    transaction = await ERC20TokenNML.connect(deployer).transfer(user3.address, amount2)
    await transaction.wait()
    console.log(`Transferred ${amount2} of NML \n from ${deployer.address} \n to ${user3.address}\n`)

    // 1. Deposit Tokens
    // Now each of the users (1-3) has 2 YAK, 250000 IMA and 250000 NML
    // The deployer has 5 YAK, 250000 IMA and 250000 NML
    amount1 = convertToWei(1)
    amount2 = convertToWei(100000)

    // User 3 Approves and Deposits 1 YAK
    transaction = await ERC20TokenYAK.connect(user3).approve(ERC20DecEx.address, amount1)
    await transaction.wait()
    console.log(`Approved ${amount1} of YAK \n from ${user3.address}\n`)

    transaction = await ERC20DecEx.connect(user3).depositTokens(ERC20TokenYAK.address, amount1)
    await transaction.wait()
    console.log(`Deposited ${amount1} of YAK \n from ${user3.address}\n`)

    // User 1 Approves and Deposits 100000 IMA
    transaction = await ERC20TokenIMA.connect(user1).approve(ERC20DecEx.address, amount2)
    await transaction.wait()
    console.log(`Approved ${amount2} of IMA \n from ${user1.address}\n`)

    transaction = await ERC20DecEx.connect(user1).depositTokens(ERC20TokenIMA.address, amount2)
    await transaction.wait()
    console.log(`Deposited ${amount2} of IMA \n from ${user1.address}\n`)

    // User 2 Approves and Deposits 100000 NML 
    transaction = await ERC20TokenNML.connect(user2).approve(ERC20DecEx.address, amount2)
    await transaction.wait()
    console.log(`Approved ${amount2} of NML \n from ${user2.address}\n`)

    transaction = await ERC20DecEx.connect(user2).depositTokens(ERC20TokenNML.address, amount2)
    await transaction.wait()
    console.log(`Deposited ${amount2} of NML \n from ${user2.address}\n`)

    // 2. Withdraw Tokens
    // Now user 1 has deposited 100000 IMA, user 2 has deposited 100000 NML and user 3 has deposited 1 YAK
    // In their wallets, user 1 has 2 YAK, 150000 IMA and 250000 NML;
    //                   user 2 has 2 YAK, 250000 IMA and 150000 NML;
    //                   user 3 has 1 YAK, 250000 IMA and 250000 NML;
    //                   the deployer has 5 YAK, 250000 IMA and 250000 NML
    amount1 = convertToWei(1)
    amount2 = convertToWei(50000)

    // User 3 Withdraws 1 YAK; Then Approves and Deposits 1 YAK Again
    transaction = await ERC20DecEx.connect(user3).withdrawTokens(ERC20TokenYAK.address, amount1)
    await transaction.wait()
    console.log(`Withdrawn ${amount1} of YAK \n back to ${user3.address}\n`)

    transaction = await ERC20TokenYAK.connect(user3).approve(ERC20DecEx.address, amount1)
    await transaction.wait()
    console.log(`Approved ${amount1} of YAK \n from ${user3.address}\n`)

    transaction = await ERC20DecEx.connect(user3).depositTokens(ERC20TokenYAK.address, amount1)
    await transaction.wait()
    console.log(`Deposited ${amount1} of YAK \n from ${user3.address}\n`)

    // User 1 Withdraws 50000 IMA
    transaction = await ERC20DecEx.connect(user1).withdrawTokens(ERC20TokenIMA.address, amount2)
    await transaction.wait()
    console.log(`Withdrawn ${amount2} of IMA \n back to ${user1.address}\n`)

    // User 2 Withdraws 50000 NML
    transaction = await ERC20DecEx.connect(user2).withdrawTokens(ERC20TokenNML.address, amount2)
    await transaction.wait()
    console.log(`Withdrawn ${amount2} of NML \n back to ${user2.address}\n`)

    // 3. Make Orders
    // Now user 1 has deposited 50000 IMA, user 2 has deposited 50000 NML and user 3 has deposited 1 YAK
    // In their wallets, user 1 has 2 YAK, 200000 IMA and 250000 NML;
    //                   user 2 has 2 YAK, 250000 IMA and 200000 NML;
    //                   user 3 has 1 YAK, 250000 IMA and 250000 NML;
    //                   the deployer has 5 YAK, 250000 IMA and 250000 NML
    amount1 = convertToWei(10000)
    amount2 = convertToWei(1)

    let result1, result2, result3

    // User 1 Wishes to Buy 10000 NML with 10000 IMA
    transaction = await ERC20DecEx.connect(user1).makeOrder(ERC20TokenNML.address, amount1, ERC20TokenIMA.address, amount1)
    result1 = await transaction.wait()
    console.log(`Made order to get ${amount1} of NML by giving ${amount1} of IMA\n`)

    // User 2 Wishes to Buy 1 YAK with 10000 NML
    transaction = await ERC20DecEx.connect(user2).makeOrder(ERC20TokenYAK.address, amount2, ERC20TokenNML.address, amount1)
    result2 = await transaction.wait()
    console.log(`Made order to get ${amount2} of YAK by giving ${amount1} of NML\n`)

    // User 3 Wishes to Buy 10000 IMA with 1 YAK
    transaction = await ERC20DecEx.connect(user3).makeOrder(ERC20TokenIMA.address, amount1, ERC20TokenYAK.address, amount2)
    result3 = await transaction.wait()
    console.log(`Made order to get ${amount1} of IMA by giving ${amount2} of YAK\n`)

    // 4. Cancel Orders; picked the order made by user 2 to cancel
    let ordersId = result2.events[0].args._ordersId
    transaction = await ERC20DecEx.connect(user2).cancelOrder(ordersId)
    await transaction.wait()
    console.log(`Cancelled order with ID ${ordersId} by ${user2.address} \n`)

    await wait(1)

    // 5. Fill Orders
    // Now user 1 has deposited 50000 IMA, user 2 has deposited 50000 NML and user 3 has deposited 1 YAK
    // In their wallets, user 1 has 2 YAK, 200000 IMA and 250000 NML;
    //                   user 2 has 2 YAK, 250000 IMA and 200000 NML;
    //                   user 3 has 1 YAK, 250000 IMA and 250000 NML;
    //                   the deployer has 5 YAK, 250000 IMA and 250000 NML
    
    // User 2 fills the order made by user 1
    // Now, user 1 got deposit: 0 YAK, 40000 IMA, 10000 NML
    //      user 2 got deposit: 0 YAK, 10000 IMA, 40000 NML
    ordersId = result1.events[0].args._ordersId
    transaction = await ERC20DecEx.connect(user2).fillOrder(ordersId)
    await transaction.wait()
    console.log(`Filled order with ID ${ordersId} by ${user2.address} \n`)

    // User 1 fills the order made by user 3
    // Now, user 1 got deposit: 1 YAK, 30000 IMA, 10000 NML
    //      user 3 got deposit: 0 YAK, 10000 IMA, 0 NML
    ordersId = result3.events[0].args._ordersId
    transaction = await ERC20DecEx.connect(user1).fillOrder(ordersId)
    await transaction.wait()
    console.log(`Filled order with ID ${ordersId} by ${user1.address} \n`)

    // transaction = await ERC20DecEx.connect(user1).makeOrder(ERC20TokenNML.address, amount1, ERC20TokenIMA.address, amount1)
    // result = await transaction.wait()
    // console.log(`Made order to get ${amount1} of NML by giving ${amount1} of IMA\n`)

    // ordersId = result.events[0].args._ordersId
    // transaction = await ERC20DecEx.connect(user3).fillOrder(ordersId)
    // await transaction.wait()
    // console.log(`Filled order with ID ${ordersId} by ${user3.address} \n`)

    await wait(1)

    // 6. Make More Orders
    // Now, in the deposit at the Dex:
    //   user 1 got: 1 YAK, 30000 IMA, 10000 NML
    //   user 2 got: 0 YAK, 10000 IMA, 40000 NML
    //   user 3 got: 0 YAK, 10000 IMA, 0 NML
    // In their wallets:
    //   user 1 has 2 YAK, 200000 IMA and 250000 NML;
    //   user 2 has 2 YAK, 250000 IMA and 200000 NML;
    //   user 3 has 1 YAK, 250000 IMA and 250000 NML;
    //   the deployer has 5 YAK, 250000 IMA and 250000 NML
    
    // User 1 Makes More Orders (to buy NML with IMA)
    for(let i = 1; i<=5; i++) {
        transaction = await ERC20DecEx.connect(user1).makeOrder(ERC20TokenNML.address, convertToWei(100*i), ERC20TokenIMA.address, convertToWei(100))
        await transaction.wait()
        console.log(`Made ${i}-th order from ${user1.address} to buy ${convertToWei(100*i)} NML with ${convertToWei(100)} IMA`)
        await wait(1)
    }

    // User 2 Makes More Orders (to buy IMA with NML)
    for(let i = 1; i<=5; i++) {
        transaction = await ERC20DecEx.connect(user2).makeOrder(ERC20TokenIMA.address, convertToWei(100*i), ERC20TokenNML.address, convertToWei(100))
        await transaction.wait()
        console.log(`Made ${i}-th order from ${user2.address} to buy ${convertToWei(100*i)} IMA with ${convertToWei(100)} NML`)
        await wait(1)
    }

    // User 3 Makes More Orders (to buy NML with IMA)
    for(let i = 1; i<=5; i++) {
        transaction = await ERC20DecEx.connect(user3).makeOrder(ERC20TokenNML.address, convertToWei(100*i), ERC20TokenIMA.address, convertToWei(100))
        await transaction.wait()
        console.log(`Made ${i}-th order from ${user3.address} to buy ${convertToWei(100*i)} NML with ${convertToWei(100)} IMA`)
        await wait(1)
    }
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error)
        process.exit(1)
    })
