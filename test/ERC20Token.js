const { expect } = require("chai")
const { ethers } = require("hardhat")

const convertToWei = (n) => {
    return ethers.utils.parseUnits(n.toString(), "ether")
}

describe("ERC-20 Token: Imoan (IMA)", () => {
    
    let erc20Token, accounts, deployer, receiver, spender

    beforeEach(async () => {
        // Fetch the contract from the blockchain
        const ERC20Token = await ethers.getContractFactory("ERC20Token")
        
        // Deploy the ERC-20 token's smart contract specifying its name, symbol, decimals and total supply
        // For Imoan  : IMA, 18, 1000000
        // For Nomolos: NML, 18, 1000000
        erc20Token = await ERC20Token.deploy("Imoan", "IMA", 18, 1000000)

        accounts = await ethers.getSigners()
        deployer = accounts[0]
        receiver = accounts[1]
        spender = accounts[2]
    })

    describe("Deployment of Imoan (IMA)", () => {
        const name = "Imoan"
        const symbol = "IMA"
        const decimals = 18
        const totalSupply = convertToWei(1000000)

        it("is named Imoan", async () => {
            // const name = await tokenIMA.name()
            // expect(name).to.equal("Imoan")
            expect(await erc20Token.name()).to.equal(name)
        })

        it("has symbol called IMA", async () => {
            expect(await erc20Token.symbol()).to.equal(symbol)
        })

        it("has 18 decimals", async () => {
            expect(await erc20Token.decimals()).to.equal(decimals)
        })

        it("has one million total supply", async () => {
            expect(await erc20Token.totalSupply()).to.equal(totalSupply)
        })

        it("assigns total supply to the deployer", async () => {
            expect(await erc20Token.balanceOf(deployer.address)).to.equal(totalSupply)
        })
    })

    describe("Transferral of Imoan (IMA)", () => {
        let amount, transferral, result

        describe("Successful Transferral", () => {
            beforeEach(async () => {
                amount = 1
                transferral = await erc20Token.connect(deployer).transfer(receiver.address, convertToWei(amount))
                result = await transferral.wait()
            })

            it("transfers tokens successfully", async () => {
                // console.log("Deployer's balance after transferral: ", await erc20Token.balanceOf(deployer.address))
                // console.log("Receiver's balance after transferral: ", await erc20Token.balanceOf(receiver.address))
                expect(await erc20Token.balanceOf(deployer.address)).to.equal(convertToWei(1000000-amount))
                expect(await erc20Token.balanceOf(receiver.address)).to.equal(convertToWei(amount))
            })

            it("emits a Transfer event", async () => {
                const event = result.events[0]
                expect(event.event).to.equal("Transfer")

                const args = event.args
                expect(args._from).to.equal(deployer.address)
                expect(args._to).to.equal(receiver.address)
                expect(args._amount).to.equal(convertToWei(amount))
            })
        })

        describe("Failed Transferral", () => {
            it("rejects deployer for insufficient balance", async () => {
                const invalidAmount = 10000000
                await expect(erc20Token.connect(deployer).transfer(receiver.address, convertToWei(invalidAmount))).to.be.reverted
            })

            it("rejects invalid recipient", async () => {
                amount = 1
                await expect(erc20Token.connect(deployer).transfer("0x0000000000000000000000000000000000000000", convertToWei(amount))).to.be.reverted
            })
        })
    })

    describe("Approval of Imoan (IMA)", () => {
        let amount, approval, result

        beforeEach(async () => {
            amount = 10
            approval = await erc20Token.connect(deployer).approve(spender.address, convertToWei(amount))
            result = await approval.wait()
        })

        describe("Successful Approval", () => {
            it("allocates an allowance to a delegated spender", async () => {
                expect(await erc20Token.allowance(deployer.address, spender.address)).to.equal(convertToWei(amount))
            })

            it("emits an Approval event", async () => {
                const event = result.events[0]
                expect(event.event).to.equal("Approval")

                const args = event.args
                expect(args._owner).to.equal(deployer.address)
                expect(args._spender).to.equal(spender.address)
                expect(args._allowance).to.equal(convertToWei(amount))
            })
        })

        describe("Failed Approval", () => {
            it("rejects invalid spender", async () => {
                amount = 1
                await expect(erc20Token.connect(deployer).approve("0x0000000000000000000000000000000000000000", convertToWei(amount))).to.be.reverted
            })
        })
    })

    describe("Delegated Transferral of Imoan (IMA)", () => {
        let allowance, amount, approval, transferral, result

        beforeEach(async () => {
            allowance = 100
            approval = await erc20Token.connect(deployer).approve(spender.address, convertToWei(allowance))
            result = await approval.wait()
        })

        describe("Successful Delegated Transferral", () => {
            beforeEach(async () => {
                amount = 10
                transferral = await erc20Token.connect(spender).transferFrom(deployer.address, receiver.address, convertToWei(amount))
                result = await transferral.wait()
            })

            it("transfers tokens successfully on deployer's behalf", async () => {
                expect(await erc20Token.balanceOf(deployer.address)).to.equal(convertToWei(1000000-amount))
                expect(await erc20Token.balanceOf(receiver.address)).to.equal(convertToWei(amount))
            })

            it("emits a Transfer event", async () => {
                const event = result.events[0]
                expect(event.event).to.equal("Transfer")

                const args = event.args
                expect(args._from).to.equal(deployer.address)
                expect(args._to).to.equal(receiver.address)
                expect(args._amount).to.equal(convertToWei(amount))
            })

            it("resets the allowance", async () => {
                expect(await erc20Token.allowance(deployer.address, spender.address)).to.equal(convertToWei(allowance-amount))
            })
        })

        describe("Failed Delegated Transferral", () => {
            it("rejects spender for insufficient allowance", async () => {
                amount = 1000
                await expect(erc20Token.connect(spender).transferFrom(deployer.address, receiver.address, convertToWei(amount))).to.be.reverted
            })

            it("rejects deployer for insufficient balance", async () => {
                const invalidAmount = 10000000
                await expect(erc20Token.connect(spender).transferFrom(deployer.address, receiver.address, convertToWei(invalidAmount))).to.be.reverted
            })

            it("rejects invalid recipient", async () => {
                amount = 1
                await expect(erc20Token.connect(spender).transferFrom(deployer.address, "0x0000000000000000000000000000000000000000", convertToWei(amount))).to.be.reverted
            })
        })
    })
})
