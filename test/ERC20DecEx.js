const { expect } = require("chai")
const { ethers } = require("hardhat")

const convertToWei = (n) => {
    return ethers.utils.parseUnits(n.toString(), "ether")
}

describe("ERC-20 Decentralized Exchange", () => {
    
    let erc20DecEx                    // variable containing the dex's smart contract
    let erc20TokenIMA, erc20TokenNML  // variables containing the tokens' smart contracts

    const feePercent = 1                              // cannot set to 0.01 due to underflow
    let accounts, deployer, feeAccount, user1, user2  // variables containing the addresses of test accounts

    beforeEach(async () => {
        // Fetch the smart contracts of the dex and token
        const ERC20DecEx = await ethers.getContractFactory("ERC20DecEx")
        const ERC20Token = await ethers.getContractFactory("ERC20Token")

        // Set addresses of test accounts
        accounts = await ethers.getSigners()
        deployer = accounts[0]
        feeAccount = accounts[1]
        user1 = accounts[2]
        user2 = accounts[3]
        user3 = accounts[4]

        // Deploy the fetched smart contracts of the dex and tokens
        erc20DecEx = await ERC20DecEx.deploy(feeAccount.address, feePercent)
        erc20TokenIMA = await ERC20Token.deploy("Imoan", "IMA", 18, 1000000)
        erc20TokenNML = await ERC20Token.deploy("Nomolos", "NML", 18, 1000000)
    })

    describe("Deployment (via Constructor) of the Decentralized Exchange for ERC-20 Tokens", () => {
        // Check the address of the fee account set via the constructor
        it("gets the correct fee account", async () => {
            expect(await erc20DecEx.feeAccount()).to.equal(feeAccount.address)
        })

        // Check the fee percentage set via the constructor
        it("sets the correct fee percent", async () => {
            expect(await erc20DecEx.feePercent()).to.equal(feePercent)
        })

        // Check the orders counter initiated via the constructor
        it("initializes correctly the order ID", async () => {
            expect(await erc20DecEx.ordersId()).to.equal(0)
        })
    })

    describe("Deposit of Tokens to the ERC-20 Decentralized Exchange", () => {
        let transferral, approval, deposit, result
        let amount = convertToWei(100)

        describe("Succussful Deposit", () => {
            beforeEach(async () => {
                transferral = await erc20TokenIMA.connect(deployer).transfer(user1.address, amount)
                await transferral.wait()

                approval = await erc20TokenIMA.connect(user1).approve(erc20DecEx.address, amount)
                await approval.wait()

                deposit = await erc20DecEx.connect(user1).depositTokens(erc20TokenIMA.address, amount)
                result = await deposit.wait()
            })

            it("tracks the token deposit", async () => {
                expect(await erc20TokenIMA.balanceOf(erc20DecEx.address)).to.equal(amount)
                expect(await erc20TokenIMA.balanceOf(user1.address)).to.equal(0)
                expect(await erc20DecEx.depositedToken(erc20TokenIMA.address, user1.address)).to.equal(amount)
                expect(await erc20DecEx.balanceOf(erc20TokenIMA.address, user1.address)).to.equal(amount)
            })

            it("emits a Deposit event", async () => {
                const event = result.events[1]
                expect(event.event).to.equal("DepositedTokens")

                const args = event.args
                expect(args._token).to.equal(erc20TokenIMA.address)
                expect(args._user).to.equal(user1.address)
                expect(args._amount).to.equal(amount)
                expect(args._balance).to.equal(amount)
            })
        })

        describe("Failed Deposit", () => {
            it("fails when no approval is granted", async () => {
                await expect(erc20DecEx.connect(user1).depositTokens(erc20TokenIMA.address, amount)).to.be.reverted;
            })
        })
    })

    describe("Withdrawal of Tokens from the ERC-20 Decentralized Exchange", () => {
        let transferral, approval, deposit, withdrawal, result
        let amount = convertToWei(100)

        describe("Successful Withdrawal", () => {
            beforeEach(async () => {
                transferral = await erc20TokenIMA.connect(deployer).transfer(user1.address, amount)
                await transferral.wait()

                approval = await erc20TokenIMA.connect(user1).approve(erc20DecEx.address, amount)
                await approval.wait()

                deposit = await erc20DecEx.connect(user1).depositTokens(erc20TokenIMA.address, amount)
                await deposit.wait()
                
                withdrawal = await erc20DecEx.connect(user1).withdrawTokens(erc20TokenIMA.address, amount)
                result = await withdrawal.wait()
            })

            it("tracks the token withdrawal", async () => {
                expect(await erc20TokenIMA.balanceOf(erc20DecEx.address)).to.equal(0)
                expect(await erc20TokenIMA.balanceOf(user1.address)).to.equal(amount)
                expect(await erc20DecEx.depositedToken(erc20TokenIMA.address, user1.address)).to.equal(0)
                expect(await erc20DecEx.balanceOf(erc20TokenIMA.address, user1.address)).to.equal(0)
            })

            it("emits a Withdrawal event", async () => {
                // console.log(result.events[0], result.events[1], result.events[2])
                const event = result.events[1]
                expect(event.event).to.equal("WithdrawnTokens")

                const args = event.args
                expect(args._token).to.equal(erc20TokenIMA.address)
                expect(args._user).to.equal(user1.address)
                expect(args._amount).to.equal(amount)
                expect(args._balance).to.equal(0)
            })
        })

        describe("Failed Withdrawal", () => {
            it("fails when there is insufficient deposit to withdraw", async () => {
                await expect(erc20DecEx.connect(user1).withdrawTokens(erc20TokenIMA.address, amount)).to.be.reverted;
            })
        })
    })

    describe("Making Orders to the Decentralized Exchange for ERC-20 Tokens", () => {
        let transferral, approval, deposit, order, result
        let amount1 = convertToWei(10)
        let amount2 = convertToWei(1)

        describe("Successful Order Making", async () => {
            beforeEach(async () => {
                transferral = await erc20TokenIMA.connect(deployer).transfer(user1.address, amount1)
                await transferral.wait()

                approval = await erc20TokenIMA.connect(user1).approve(erc20DecEx.address, amount1)
                await approval.wait()

                deposit = await erc20DecEx.connect(user1).depositTokens(erc20TokenIMA.address, amount1)
                await deposit.wait()

                order = await erc20DecEx.connect(user1).makeOrder(erc20TokenNML.address, amount2, erc20TokenIMA.address, amount2)
                result = await order.wait()
            })

            it("tracks newly made order with ID number 1", async () => {
                expect(await erc20DecEx.ordersId()).to.equal(1)
            })

            it("emits a MadeOrder event", async () => {
                // console.log(result.events[0], result.events[1], result.events[2], result.events[3])
                const event = result.events[0]
                expect(event.event).to.equal("MadeOrder")

                const args = event.args
                expect(args._ordersId).to.equal(1)
                expect(args._user).to.equal(user1.address)
                expect(args._tokenGet).to.equal(erc20TokenNML.address)
                expect(args._amountGet).to.equal(amount2)
                expect(args._tokenGive).to.equal(erc20TokenIMA.address)
                expect(args._amountGive).to.equal(amount2)
                expect(args._timestampMade).to.at.least(1)
            })
        })

        describe("Failed Order Making", async () => {
            it("fails when there is insufficient deposit to make an order", async () => {
                await expect(erc20DecEx.connect(user1).makeOrder(erc20TokenNML.address, amount2, erc20TokenIMA.address, amount2)).to.be.reverted;
            })
        })
    })

    describe("Cancelling Orders to the Decentralized Exchange for ERC-20 Tokens", () => {
        let transferral, approval, deposit, order, cancel, result
        let amount1 = convertToWei(10)
        let amount2 = convertToWei(1)

        beforeEach(async () => {
            transferral = await erc20TokenIMA.connect(deployer).transfer(user1.address, amount1)
            await transferral.wait()

            approval = await erc20TokenIMA.connect(user1).approve(erc20DecEx.address, amount1)
            await approval.wait()

            deposit = await erc20DecEx.connect(user1).depositTokens(erc20TokenIMA.address, amount1)
            await deposit.wait()

            order = await erc20DecEx.connect(user1).makeOrder(erc20TokenNML.address, amount2, erc20TokenIMA.address, amount2)
            await order.wait()
        })

        describe("Successful Order Cancellation", async () => {
            beforeEach(async () => {
                cancel = await erc20DecEx.connect(user1).cancelOrder(erc20DecEx.ordersId())
                result = await cancel.wait()
            })

            it("tracks newly cancelled order", async () => {
                // The following statement does not work:

                // expect(await erc20DecEx.order(erc20DecEx.ordersId())._status).to.equal("Cancelled")

                // It is guessed the reason is that we have to fetch the object (i.e. the order struct)
                // first; and then the element in it. In other words, we have to do it in two steps, like:

                const orderObject = await erc20DecEx.order(erc20DecEx.ordersId())
                expect(orderObject._status).to.equal("Cancelled")
            })

            it("emits a CancelledOrder event", async () => {
                const event = result.events[0]
                expect(event.event).to.equal("CancelledOrder")

                const args = event.args
                expect(args._ordersId).to.equal(1)
                expect(args._user).to.equal(user1.address)
                expect(args._tokenGet).to.equal(erc20TokenNML.address)
                expect(args._amountGet).to.equal(amount2)
                expect(args._tokenGive).to.equal(erc20TokenIMA.address)
                expect(args._amountGive).to.equal(amount2)
                expect(args._timestampMade).to.at.least(1)
                expect(args._timestampCancelled).to.at.least(1)
                // expect(args._hoursLived).to.greaterThan(0)
            })
        })

        describe("Failed Order Cancellation", async () => {
            it("rejects invalid user to cancel an existing order", async () => {
                await expect(erc20DecEx.connect(user2).cancelOrder(erc20DecEx.ordersId())).to.be.reverted
            })

            it("rejects cancelling order that does not exist", async () => {
                const invalidId = 999
                await expect(erc20DecEx.connect(user1).cancelOrder(invalidId)).to.be.reverted
            })

            it("rejects cancelling a non-pending order", async () => {
                cancel = await erc20DecEx.connect(user1).cancelOrder(erc20DecEx.ordersId())
                await cancel.wait()

                await expect(erc20DecEx.connect(user1).cancelOrder(erc20DecEx.ordersId())).to.be.reverted
            })
        })
    })

    describe("Filling Orders to the Decentralized Exchange for ERC-20 Tokens", () => {
        let transferral1, transferral2, approval1, approval2, deposit1, deposit2, order, fill, cancel, result
        let amount1 = convertToWei(10)
        let amount2 = convertToWei(1)

        beforeEach(async () => {
            transferral1 = await erc20TokenIMA.connect(deployer).transfer(user1.address, amount1)
            await transferral1.wait()

            transferral2 = await erc20TokenNML.connect(deployer).transfer(user2.address, amount1)
            await transferral2.wait()

            approval1 = await erc20TokenIMA.connect(user1).approve(erc20DecEx.address, amount1)
            await approval1.wait()

            approval2 = await erc20TokenNML.connect(user2).approve(erc20DecEx.address, amount1)
            await approval2.wait()

            deposit1 = await erc20DecEx.connect(user1).depositTokens(erc20TokenIMA.address, amount1)
            await deposit1.wait()

            deposit2 = await erc20DecEx.connect(user2).depositTokens(erc20TokenNML.address, amount1)
            await deposit2.wait()

            order = await erc20DecEx.connect(user1).makeOrder(erc20TokenNML.address, amount2, erc20TokenIMA.address, amount2)
            await order.wait()
        })


        describe("Successful Order Filling", async () => {
            beforeEach(async () => {
                fill = await erc20DecEx.connect(user2).fillOrder(erc20DecEx.ordersId())
                result = await fill.wait()
            })

            it("fills order and pays the fee to the feeAccount of the exchange", async () => {
                expect(await erc20DecEx.depositedToken(erc20TokenIMA.address, user1.address)).to.equal(convertToWei(9))
                expect(await erc20DecEx.depositedToken(erc20TokenNML.address, user1.address)).to.equal(convertToWei(1))

                expect(await erc20DecEx.depositedToken(erc20TokenIMA.address, user2.address)).to.equal(convertToWei(1))
                expect(await erc20DecEx.depositedToken(erc20TokenNML.address, user2.address)).to.equal(convertToWei(8.99))

                expect(await erc20TokenNML.balanceOf(feeAccount.address)).to.equal(convertToWei(0.01))
                expect(await erc20TokenIMA.balanceOf(feeAccount.address)).to.equal(0)
            })

            it("emits a FilledOrder event", async () => {
                const event = result.events[1]
                expect(event.event).to.equal("FilledOrder")

                const args = event.args
                expect(args._ordersId).to.equal(1)
                expect(args._userMade).to.equal(user1.address)
                expect(args._userFilled).to.equal(user2.address)
                expect(args._tokenGet).to.equal(erc20TokenNML.address)
                expect(args._amountGet).to.equal(amount2)
                expect(args._tokenGive).to.equal(erc20TokenIMA.address)
                expect(args._amountGive).to.equal(amount2)
                expect(args._timestampMade).to.at.least(1)
                expect(args._timestampFilled).to.at.least(1)
                // expect(args._hoursLived).to.greaterThan(0)
            })
        })

        describe("Failed Order Filling", async () => {
            it("rejects filling order that does not exist", async () => {
                const invalidId = 999
                await expect(erc20DecEx.connect(user2).fillOrder(invalidId)).to.be.reverted
            })

            it("rejects filling a cancelled order", async () => {
                cancel = await erc20DecEx.connect(user1).cancelOrder(erc20DecEx.ordersId())
                await cancel.wait()

                await expect(erc20DecEx.connect(user2).fillOrder(erc20DecEx.ordersId())).to.be.reverted
            })

            it("rejects filling a filled order", async () => {
                fill = await erc20DecEx.connect(user2).fillOrder(erc20DecEx.ordersId())
                await fill.wait()

                await expect(erc20DecEx.connect(user3).fillOrder(erc20DecEx.ordersId())).to.be.reverted
            })

            it("rejects the order maker himself/herself filling the order", async () => {
                await expect(erc20DecEx.connect(user1).fillOrder(erc20DecEx.ordersId())).to.be.reverted
            })

            it("rejects filling order due to insufficient balance", async () => {
                let withdrawal
                withdrawal = await erc20DecEx.connect(user2).withdrawTokens(erc20TokenNML.address, convertToWei(9))
                await withdrawal.wait()

                await expect(erc20DecEx.connect(user2).fillOrder(erc20DecEx.ordersId())).to.be.reverted
            })
        })
    })
})
