import { useRef, useEffect } from "react";
import { useSelector } from "react-redux";

import { myEventsSelector } from "../store/selectors";

import config from "../config.json";

const Notifications = () => {
    const notificationRef = useRef(null)
    
    const network = useSelector(state => state.provider.network)
    const account = useSelector(state => state.provider.account)
    const isPending = useSelector(state => state.decex.transaction.isPending)
    const isError = useSelector(state => state.decex.transaction.isError)
    const myEvents = useSelector(myEventsSelector)

    const handleRemoval = async (e) => {
        notificationRef.current.className = "alert--remove"
    }

    useEffect(() => {
        if(account && (myEvents[0] || isPending || isError)) {
            notificationRef.current.className = "alert"
        }
    }, [myEvents, isPending, isError, account])

    return (
      <div>
          {isPending ? (
            <div className="alert alert--remove" onClick={handleRemoval} ref={notificationRef}>
              <h1>Transaction Pending...</h1>
            </div>
          ) : isError ? (
            <div className="alert alert--remove" onClick={handleRemoval} ref={notificationRef}>
              <h1>Transaction Will Fail</h1>
            </div>
          ) : myEvents[0] && !isPending ? (
            <div className="alert alert--remove" onClick={handleRemoval} ref={notificationRef}>
              <h1>Transaction Successful</h1>
                <a
                  href={config[network] ? `${config[network].explorerURL}/tx/${myEvents[0].transactionHash}` : "#"}
                  target='_blank'
                  rel='noreferrer'
                >
                    {myEvents[0].transactionHash.slice(0, 6) + "..." + myEvents[0].transactionHash.slice(60, 66)}
                </a>
            </div>
          ) : (
            <div></div>
          )}
      </div>
    );
  }
  
  export default Notifications;
