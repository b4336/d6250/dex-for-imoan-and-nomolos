import { useSelector, useDispatch } from "react-redux";
import config from "../config.json";
import { loadTokens } from "../store/dispatches";

const Markets = () => {
    const provider = useSelector(state => state.provider.connection)
    const chainId = useSelector(state => state.provider.network)

    // Intended to use the following variables in the return statement, but fails for unknown reason
    // const YAK = config[chainId].ERC20TokenYAK
    // const IMA = config[chainId].ERC20TokenIMA
    // const NML = config[chainId].ERC20TokenNML

    const dispatch = useDispatch()

    const switchMarket = async (event) => {
        loadTokens(provider, (event.target.value).split(", "), dispatch)
    }

    return (
        <div className="component exchange__markets">
            <div className="component__header">
                <h2>Market</h2>
                <small>Base / Quote</small>
            </div>
            {chainId && config[chainId] ? (
                <select name="markets" id="markets" onChange={switchMarket}>
                    <option value={`${config[chainId].ERC20TokenYAK.address}, ${config[chainId].ERC20TokenIMA.address}`}>YAK / IMA (Default)</option>
                    <option value={`${config[chainId].ERC20TokenYAK.address}, ${config[chainId].ERC20TokenNML.address}`}>YAK / NML</option>
                    <option value={`${config[chainId].ERC20TokenIMA.address}, ${config[chainId].ERC20TokenNML.address}`}>IMA / NML</option>
                </select>
            ) : (
                <div>
                    <p>Not Deployed to a Network.</p>
                </div>
            )}
            <hr />
        </div>
    )
}

export default Markets;
