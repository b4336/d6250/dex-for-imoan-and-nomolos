// import '../assets/YAK.png';
// import '../assets/IMA.png';
// import '../assets/NML.png';
import { useEffect, useState, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
    loadDecExBalances,
    transferTokens
} from "../store/dispatches";

const Balances = () => {
    const provider = useSelector(state => state.provider.connection)
    const account = useSelector(state => state.provider.account)

    const tokens = useSelector(state => state.tokens.contracts)
    const symbols = useSelector(state => state.tokens.symbols)
    const tokenBalances = useSelector(state => state.tokens.balances)

    const dex = useSelector(state => state.decex.contract)
    const dexBalances = useSelector(state => state.decex.balances)
    const transferInProgress = useSelector(state => state.decex.transferInProgress)

    const [isDeposit, setIsDeposit] = useState(true)
    const [token0TransferAmount, setToken0TransferAmount] = useState(0)
    const [token1TransferAmount, setToken1TransferAmount] = useState(0)

    const dispatch = useDispatch()

    useEffect(() => {
      if (dex && tokens[0] && tokens[1] && account) {
          loadDecExBalances(dex, tokens, account, dispatch)
      }
    }, [dex, tokens, account, transferInProgress, dispatch])  // Loads the loadDecExBalances() again if anything in the array changes

    const depositRef = useRef(null)
    const withdrawRef = useRef(null)

    const handleTab = (e) => {
      if (e.target.className !== depositRef.current.className) { // If the withdraw tab is clicked...
        e.target.className = "tab tab--active"                   // then make the withdraw tab active
        depositRef.current.className = "tab"                     // and make the deposit tab inactive
        setIsDeposit(false)
      } else {                                                   // If the deposit tab is clicked...
        e.target.className = "tab tab--active"                   // then make the deposit tab active
        withdrawRef.current.className = "tab"                    // and make the withdraw tab inactive
        setIsDeposit(true)
      }
    }

    const handleTransferAmount = (e, token) => {
      if (token.address === tokens[0].address) {
          setToken0TransferAmount(e.target.value)
      } else {
          setToken1TransferAmount(e.target.value)
      }
    }

    const handleDeposit = (e, token) => {
      e.preventDefault() // Prevents refreshing the page when the button is clicked
      if (token.address === tokens[0].address) {
        if (isNaN(token0TransferAmount)) {
          window.alert(`You have entered a non-number. Please double-check and re-enter again.`)
        } else if (Number(tokenBalances[0]) < Number(token0TransferAmount)) {
          window.alert(`You do not have sufficient ${symbols[0]} balance in your wallet. Please double-check and re-enter again.`)
        } else {
          transferTokens(provider, dex, "Deposit", token, token0TransferAmount, dispatch)
          setToken0TransferAmount(0)
        }
      } else {
        if (isNaN(token1TransferAmount)) {
          window.alert(`You have entered a non-number. Please double-check and re-enter again.`)
        } else if (Number(tokenBalances[1]) < Number(token1TransferAmount)) {
          window.alert(`You do not have sufficient ${symbols[1]} balance in your wallet. Please double-check and re-enter again.`)
        } else {
          transferTokens(provider, dex, "Deposit", token, token1TransferAmount, dispatch)
          setToken1TransferAmount(0)
        }
      }
    }

    const handleWithdrawal = (e, token) => {
      e.preventDefault() // Prevents refreshing the page when the button is clicked
      if (token.address === tokens[0].address) {
        if (isNaN(token0TransferAmount)) {
          window.alert(`You have entered a non-number. Please double-check and re-enter again.`)
        } else if (Number(dexBalances[0]) < Number(token0TransferAmount)) {
          window.alert(`You do not have sufficient ${symbols[0]} balance in your YINdex account. Please double-check and re-enter again.`)
        } else {
          transferTokens(provider, dex, "Withdrawal", token, token0TransferAmount, dispatch)
          setToken0TransferAmount(0)
        }
      } else {
        if (isNaN(token1TransferAmount)) {
          window.alert(`You have entered a non-number. Please double-check and re-enter again.`)
        } else if (Number(dexBalances[1]) < Number(token1TransferAmount)) {
          window.alert(`You do not have sufficient ${symbols[1]} balance in your YINdex account. Please double-check and re-enter again.`)
        } else {
          transferTokens(provider, dex, "Withdrawal", token, token1TransferAmount, dispatch)
          setToken1TransferAmount(0)
        }
      }
    }

    return (
      <div className='component exchange__transfers'>
        <div className='component__header flex-between'>
          <h2>Balances</h2>
          <div className='tabs'>
            <button onClick={handleTab} ref={depositRef} className='tab tab--active'>Deposit</button>
            <button onClick={handleTab} ref={withdrawRef} className='tab'>Withdraw</button>
          </div>
        </div>

        {/* Deposit/Withdraw the Base Token */}
  
        <div className='exchange__transfers--form'>
          <div className='flex-between'>
            {/* Unknown why this fails: src={require(`../assets/${symbols[0]}.png`)} */}
            <p><small>Base Token</small><br /><img className="logo" src={symbols[0] === "YAK" ? require("../assets/YAK.png") : (symbols[0] === "IMA" ? require("../assets/IMA.png") : require("../assets/NML.png"))} alt="Base token logo." />{symbols && symbols[0]}</p>
            <p><small>Wallet</small><br />{tokenBalances && tokenBalances[0]}</p>
            <p><small>YINdex</small><br />{dexBalances && dexBalances[0]}</p>
          </div>
  
          <form onSubmit={isDeposit ? (e) => handleDeposit(e, tokens[0]) : (e) => handleWithdrawal(e, tokens[0])}>
            <label htmlFor="token0"><small>{symbols && symbols[0]} Amount to {isDeposit ? "Deposit to YINdex from Your Wallet" : "Withdraw from YINdex to Your Wallet"}</small></label>
            <input
              type="text"
              id='token0'
              placeholder={`0.0000 ${symbols[0]}`}
              value={token0TransferAmount === 0 ? "" : token0TransferAmount}
              onChange={(e) => handleTransferAmount(e, tokens[0])}
            />

            <button className='button' type='submit'>
              <span>{isDeposit ? "Deposit to YINdex from Your Wallet" : "Withdraw from YINdex to Your Wallet"}</span>
            </button>
          </form>
        </div>
  
        <hr />
  
        {/* Deposit/Withdraw the Quote Token */}
  
        <div className='exchange__transfers--form'>
          <div className='flex-between'>
            {/* Unknown why this fails: src={require(`../assets/${symbols[1]}.png`)} */}
            <p><small>Quote Token</small><br /><img className="logo" src={symbols[1] === "YAK" ? require("../assets/YAK.png") : (symbols[1] === "IMA" ? require("../assets/IMA.png") : require("../assets/NML.png"))} alt="Quote token logo." />{symbols && symbols[1]}</p>
            <p><small>Wallet</small><br />{tokenBalances && tokenBalances[1]}</p>
            <p><small>YINdex</small><br />{dexBalances && dexBalances[1]}</p>
          </div>
  
          <form onSubmit={isDeposit ? (e) => handleDeposit(e, tokens[1]) : (e) => handleWithdrawal(e, tokens[1])}>
            <label htmlFor="token1"><small>{symbols && symbols[1]} Amount to {isDeposit ? "Deposit to YINdex from Your Wallet" : "Withdraw from YINdex to Your Wallet"}</small></label>
            <input
              type="text"
              id='token1'
              placeholder={`0.0000 ${symbols[1]}`}
              value={token1TransferAmount === 0 ? "" : token1TransferAmount}
              onChange={(e) => handleTransferAmount(e, tokens[1])}
            />
  
            <button className='button' type='submit'>
              <span>{isDeposit ? "Deposit To YINdex from Your Wallet" : "Withdraw from YINdex to Your Wallet"}</span>
            </button>
          </form>
        </div>
  
        <hr />
      </div>
    );
  }
  
  export default Balances;
