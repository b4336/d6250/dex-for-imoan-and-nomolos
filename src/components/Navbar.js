import { useSelector, useDispatch } from 'react-redux'
import Blockies from 'react-blockies'

import logo from '../assets/mother.png'
import eth from '../assets/eth.svg'

import config from '../config.json'

import { loadAccountWithBalance } from '../store/dispatches'

const Navbar = () => {
    const provider = useSelector(state => state.provider.connection)
    const chainId = useSelector(state => state.provider.network)
    const account = useSelector(state => state.provider.account)
    const balance = useSelector(state => state.provider.balance)

    const dispatch = useDispatch()

    const switchChain = async (event) => {
        await window.ethereum.request({
            method: "wallet_switchEthereumChain",
            params: [{ chainId: event.target.value }]
        })
    }

    const connectWallet = async () => {
        await loadAccountWithBalance(provider, dispatch)
        // console.log(provider, account, balance, accountAndBalance)
    }

    return (
        <div className="exchange__header grid">
            <div className="exchange__header--brand flex">
                <img src={logo} className="logo" alt="Logo for the dex."></img>
                <h1>YINdex | dex for YAK, IMA & NML</h1>
            </div>

            <div className="exchange__header--networks flex">
                <img src={eth} className="Eth Logo" alt="Logo for ETH."></img>
                {chainId && (
                    <select
                        name="networks"
                        id="networks"
                        value={config[chainId] ? `0x${chainId.toString(16)}` : `0`}
                        onChange={switchChain}
                    >
                        <option value="0" disabled>Select Blockchain</option>
                        <option value="0x7A69">Localhost</option>
                        <option value="0x13881">Polygon Mumbai</option>
                        <option value="0x5">G{"\xF6"}rli</option>
                        <option value="0x2a">Kovan (deprecated)</option>
                    </select>
                )}
            </div>

            <div className="exchange__header--account flex">
                {balance ? (
                    <p><small>ETH Balance</small>{Number(balance).toFixed(4)} ETH</p>
                ) : (
                    <p><small>Wallet Not Connected</small></p>
                )}
                {account ? (
                    <a
                        href={config[chainId] ? `${config[chainId].explorerURL}/address/${account}` : `#`}
                        target="_blank"
                        rel="noreferrer"
                    >
                        <small>Address</small>
                        {account.slice(0,5) + "..." + account.slice(38,42)}
                        <Blockies
                            seed={account}
                            size={10}
                            scale={3}
                            color="#2187D0"
                            bgColor="#F1F2F9"
                            spotColor="#767F92"
                            className="identicon" 
                        />
                    </a>
                ) : (
                    <button className="button" onClick={connectWallet}>Connect to Wallet</button>
                )}
            </div>
        </div>
    )
}

export default Navbar;
