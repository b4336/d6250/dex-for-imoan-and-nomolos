import { useSelector, useDispatch } from "react-redux";

import sort from "../assets/sort.svg";

import { orderBookSelector } from "../store/selectors";

import { fillOrder } from "../store/dispatches";

const OrderBook = () => {
    const provider = useSelector(state => state.provider.connection)
    const symbols = useSelector(state => state.tokens.symbols)
    const decex = useSelector(state => state.decex.contract)
    const orderBook = useSelector(orderBookSelector)

    const dispatch = useDispatch()

    const handleFillOrder = (order) => {
      fillOrder(provider, decex, order, dispatch)
    }

    return (
      <div className="component exchange__orderbook">
        <div className='component__header flex-between'>
          <h2>Order Book</h2>
        </div>
  
        <div className="flex">
  
          {!orderBook || orderBook.sellOrders.length === 0 ? (
            <p className="flex-center">No sell orders yet in this market.</p>
          ) : (
            <table className='exchange__orderbook--sell'>
              <caption>Sell Orders</caption>
              <thead>
                <tr>
                  <th>{symbols && symbols[0]}<img src={sort} alt="Sort." /></th>
                  <th>{symbols && symbols[0]}/{symbols && symbols[1]} (Unit Price)<img src={sort} alt="Sort." /></th>
                  <th>{symbols && symbols[1]}<img src={sort} alt="Sort." /></th>
                </tr>
              </thead>
              <tbody>
                {orderBook && orderBook.sellOrders.map((order, index) => {
                  return(
                    <tr key={index} onClick={() => handleFillOrder(order)}>
                      <td>{order.baseTokenAmount}</td>
                      <td style={{ color: `${order.orderTypeClass}` }}>{order.tokenUnitPrice}</td>
                      <td>{order.quoteTokenAmount}</td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          )}
  
          <div className='divider'></div>
  
          {!orderBook || orderBook.buyOrders.length === 0 ? (
            <p className="flex-center">No buy orders yet in this market.</p>
          ) : (
            <table className='exchange__orderbook--buy'>
              <caption>Buy Orders</caption>
              <thead>
                <tr>
                  <th>{symbols && symbols[0]}<img src={sort} alt="Sort." /></th>
                  <th>{symbols && symbols[0]}/{symbols && symbols[1]} (Unit Price)<img src={sort} alt="Sort." /></th>
                  <th>{symbols && symbols[1]}<img src={sort} alt="Sort." /></th>
                </tr>
              </thead>
              <tbody>
                {orderBook && orderBook.buyOrders.map((order, index) => {
                  return(
                    <tr key={index} onClick={() => handleFillOrder(order)}>
                      <td>{order.baseTokenAmount}</td>
                      <td style={{ color: `${order.orderTypeClass}` }}>{order.tokenUnitPrice}</td>
                      <td>{order.quoteTokenAmount}</td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          )}

        </div>
      </div>
    );
  }
  
  export default OrderBook;
