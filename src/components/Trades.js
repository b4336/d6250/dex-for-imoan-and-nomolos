import { useSelector } from "react-redux";
import { filledOrdersSelector } from "../store/selectors";
import Banner from "./Banner";
import sort from "../assets/sort.svg";

const Trades = () => {
    const symbols = useSelector(state => state.tokens.symbols)
    const filledOrders = useSelector(filledOrdersSelector)

    return (
      <div className="component exchange__trades">
        <div className='component__header flex-between'>
          <h2>All Trades</h2>
        </div>
  
        {!filledOrders || filledOrders.length === 0 ? (
            <Banner text={"No transactions yet in this market."} />
        ) : (
            <table>
                <thead>
                    <tr>
                        <th>Time <img src={sort} alt="Sort." /></th>
                        <th>{symbols && symbols[0]}<img src={sort} alt="Sort." /></th>
                        <th>{symbols && `${symbols[0]}/${symbols[1]}`}<img src={sort} alt="Sort." /></th>
                    </tr>
                </thead>
                <tbody>
                    {filledOrders && filledOrders.map((order, index) => {
                        return(
                        <tr key={index}>
                            <td>{order.formattedTimestamp}</td>
                            <td style={{ color: `${order.tokenPriceClass}` }}>{order.baseTokenAmount}</td>
                            <td>{order.tokenUnitPrice}</td>
                        </tr>
                        )
                    })}
                </tbody>
            </table>
        )}
  
        </div>
    );
}
  
export default Trades;
