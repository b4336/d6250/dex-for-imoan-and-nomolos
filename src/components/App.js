import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
// import { ethers } from 'ethers';
import config from '../config.json';
// import TOKEN_ABI from '../abis/ERC20Token.json';
// import DECEX_ABI from '../abis/ERC20DecEx.json';

import { 
  loadProvider,
  loadNetwork,
  loadAccountWithBalance,
  loadTokens,
  loadDecEx,
  loadAllOrders,
  subscribeToEvents
} from '../store/dispatches'

import Navbar from './Navbar';
import Markets from './Markets';
import Balances from './Balances';
import Order from './Order';
import PriceChart from './PriceChart';
import MyRecord from './MyRecord';
import Trades from './Trades';
import OrderBook from './OrderBook';
import Notifications from './Notifications';

// import '../App.css';
// import '../assets/right-arrow-blue.png';
// import '../assets/right-arrow-white.png';

function App() {
  
  const dispatch = useDispatch()

  const loadBlockchainData = async () => {
    // Connect to the blockchain & dispatch the data to the Redux store; the same as:
    // const provider = new ethers.providers.Web3Provider(window.ethereum)
    // dispatch({ type: "PROVIDER_LOADED", connection: provider })
    const provider = loadProvider(dispatch)                    

    // Fetch current network's chainID (e.g. Hardhat: 31337, Kovan: 42); the same as:
    // const { chainId } = await provider.getNetwork()
    // dispatch({ type: "NETWORK_LOADED", chainId })
    const chainId = await loadNetwork(provider, dispatch)

    // Reload page when the network changes
    window.ethereum.on("chainChanged", () => {
      window.location.reload()
    })

    // Make a RPC call to a node to get MetaMask account & ETH balance
    // Automatically fetch the new account when the account is changed on MetaMask
    window.ethereum.on("accountsChanged", () => {
      // const accountAndBalance = loadAccountWithBalance(provider, dispatch)
      loadAccountWithBalance(provider, dispatch)
    })

    // Fetch smart contracts of the tokens
    // const symbols = useSelector(state => state.tokens.symbols) can't work?
    // Because the Selector needs to load tokens in the first place...?
    const YAK = config[chainId].ERC20TokenYAK
    const IMA = config[chainId].ERC20TokenIMA
    const defaultTokensToLoad = [YAK.address, IMA.address]
    await loadTokens(provider, defaultTokensToLoad, dispatch)

    // Fetch smart contract of the dex; the same as: 
    // const decex = new ethers.Contract(config[chainId].ERC20DecEx.address, DECEX_ABI, provider)
    // dispatch({ type: "DECEX_LOADED", decex })
    const decex = await loadDecEx(provider, config[chainId].ERC20DecEx.address, dispatch)
    await loadAllOrders(provider, decex, dispatch)
    subscribeToEvents(decex, dispatch)

    // Use the following console.log to check things when necessary
    // console.log(provider, chainId, accountAndBalance, decex.address, [IMA.address, NML.address])
  }

  useEffect(() => {
    loadBlockchainData()
  })

  return (
    <div>

      <Navbar />

      <main className="exchange grid">
        <section className="exchange__section--left grid">

          <Markets />
        
          <Balances />
        
          <Order />
        
        </section>
        <section className="exchange__section--right grid">

          <PriceChart />
        
          <MyRecord />
        
          <Trades />
        
          <OrderBook />
        
        </section>
      </main>

      <Notifications />
    </div>
  );
}

export default App;
