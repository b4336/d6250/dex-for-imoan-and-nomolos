import { useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
    makeBuyOrder,
    makeSellOrder
} from "../store/dispatches";

const Order = () => {
    const [isBuy, setIsBuy] = useState(true)
    const [amount, setAmount] = useState(0)
    const [price, setPrice] = useState(0)

    const provider = useSelector(state => state.provider.connection)
    const tokens = useSelector(state => state.tokens.contracts)
    const symbols = useSelector(state => state.tokens.symbols)
    const exchange = useSelector(state => state.decex.contract)

    const buyRef = useRef(null)
    const sellRef = useRef(null)

    const dispatch = useDispatch()

    const handleTab = (e) => {
      if (e.target.className !== buyRef.current.className) { // If the sell tab is clicked...
        e.target.className = "tab tab--active"               // then make the sell tab active
        buyRef.current.className = "tab"                     // and make the buy tab inactive
        setIsBuy(false)
      } else {                                               // If the buy tab is clicked...
        e.target.className = "tab tab--active"               // then make the buy tab active
        sellRef.current.className = "tab"                    // and make the sell tab inactive
        setIsBuy(true)
      }
    }

    // const handleAmount = () => {
    //     //
    // }

    // const handlePrice = () => {
    //     //
    // }

    const handleBuy = (e) => {
        e.preventDefault() // Prevents refreshing the page when the button is clicked
        makeBuyOrder(provider, exchange, tokens, { amount, price }, dispatch)
        setAmount(0)
        setPrice(0)
    }

    const handleSell = (e) => {
        e.preventDefault() // Prevents refreshing the page when the button is clicked
        makeSellOrder(provider, exchange, tokens, { amount, price }, dispatch)
        setAmount(0)
        setPrice(0)
    }

    return (
        <div className="component exchange__orders">
            <div className='component__header flex-between'>
                <h2>New Order</h2>
                <div className='tabs'>
                    <button onClick={handleTab} ref={buyRef} className='tab tab--active'>Buy</button>
                    <button onClick={handleTab} ref={sellRef} className='tab'>Sell</button>
                </div>
            </div>

            <form onSubmit={isBuy ? (e) => handleBuy(e) : (e) => handleSell(e)}>
                {isBuy? (
                    <label htmlFor="amount"><small>Amount to Buy (in terms of the Base Token: {symbols && symbols[0]})</small></label>
                ) : (
                    <label htmlFor="amount"><small>Amount to Sell (in terms of the Base Token: {symbols && symbols[0]})</small></label>
                )}

                <input
                    type="text"
                    id='amount'
                    placeholder={`0.0000 ${symbols[0]}`}
                    value={amount === 0 ? "" : amount}
                    onChange={(e) => setAmount(e.target.value)}
                />

                {isBuy? (
                    <label htmlFor="price"><small>Unit Price to Buy at (in terms of the Quote Token: {symbols && symbols[1]})</small><br /><small>i.e. you need to pay: {price*amount} {symbols[1]}</small></label>
                ) : (
                    <label htmlFor="price"><small>Unit Price to Sell at (in terms of the Quote Token: {symbols && symbols[1]})</small><br /><small>i.e. you may receive: {price*amount} {symbols[1]}</small></label>
                )}

                <input
                    type="text"
                    id='price'
                    placeholder={`0.0000 ${symbols[1]} per ${symbols[0]}`}
                    value={price === 0 ? "" : price}
                    onChange={(e) => setPrice(e.target.value)}
                />

                <button className='button button--filled' type='submit'>
                    <span>{isBuy ? "Make Buy Order" : "Make Sell Order"}</span>
                </button>
            </form>
        </div>
    );
}

export default Order;
