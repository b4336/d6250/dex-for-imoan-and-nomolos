import { useRef, useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import { myOrdersSelector, myTradesSelector } from "../store/selectors";

import { cancelOrder } from "../store/dispatches";

import sort from "../assets/sort.svg";

import Banner from "./Banner";

const MyRecord = () => {
    const provider = useSelector(state => state.provider.connection)
    const symbols = useSelector(state => state.tokens.symbols)
    const decex = useSelector(state => state.decex.contract)
    const myOrders = useSelector(myOrdersSelector)
    const myTrades = useSelector(myTradesSelector)

    const orderRef = useRef(null)
    const tradeRef = useRef(null)

    const [showingMyOrders, setShowingMyOrders] = useState(true)

    const dispatch = useDispatch()

    const handleTab = (e) => {
        if (e.target.className !== orderRef.current.className) {
            e.target.className = "tab tab--active"
            orderRef.current.className = "tab"
            setShowingMyOrders(false)
        } else{
            e.target.className = "tab tab--active"
            tradeRef.current.className = "tab"
            setShowingMyOrders(true)
        }
    }

    const handleCancel = (order) => {
      cancelOrder(provider, decex, order, dispatch)
    }

    return (
      <div className="component exchange__transactions">
          {showingMyOrders ? (
            <div>
              <div className='component__header flex-between'>
                <h2>My Orders</h2>
  
                <div className='tabs'>
                  <button onClick={handleTab} ref={orderRef} className='tab tab--active'>Orders</button>
                  <button onClick={handleTab} ref={tradeRef} className='tab'>Trades</button>
                </div>
              </div>
  
              {!myOrders || myOrders.length === 0 ? (
                <Banner text={"You have no orders yet in this market."} />
              ) : (
                <table>
                  <thead>
                    <tr>
                      <th>{symbols && symbols[0]}<img src={sort} alt="Sort." /></th>
                      <th>{symbols && `${symbols[0]}/${symbols[1]}` }<img src={sort} alt="Sort." /></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {myOrders && myOrders.map((order, index) => {
                        return(
                            <tr key={index}>
                              <td style={{ color: `${order.orderTypeClass}` }}>{order.baseTokenAmount}</td>
                              <td>{order.tokenUnitPrice}</td>
                              <td><button className="button--sm" onClick={() => handleCancel(order)}>Cancel</button></td>
                            </tr>
                        )
                    })}
                  </tbody>
                </table>
              )}
            </div>
          ) : (
            <div>
              <div className='component__header flex-between'>
                <h2>My Trades</h2>
  
                <div className='tabs'>
                  <button onClick={handleTab} ref={orderRef} className='tab tab--active'>Orders</button>
                  <button onClick={handleTab} ref={tradeRef} className='tab'>Trades</button>
                </div>
              </div>
  
              {!myTrades || myTrades.length === 0 ? (
                <Banner text={"You have no trades yet in this market."} />
              ) : (
                <table>
                  <thead>
                    <tr>
                      <th>Time<img src={sort} alt="Sort." /></th>
                      <th>{symbols && symbols[0]}<img src={sort} alt="Sort." /></th>
                      <th>{symbols && `${symbols[0]}/${symbols[1]}` }<img src={sort} alt="Sort." /></th>
                    </tr>
                  </thead>
                  <tbody>
                    {myTrades && myTrades.map((order, index) => {
                        return(
                            <tr key={index}>
                                <td>{order.formattedTimestamp}</td>
                                <td style={{ color: `${order.orderTypeClass}` }}>{order.orderTypeSign}{order.baseTokenAmount}</td>
                                <td>{order.tokenUnitPrice}</td>
                            </tr>
                        )
                    })}
                  </tbody>
                </table>
              )}
            </div>
          )}
      </div>
    )
  }
  
  export default MyRecord;
