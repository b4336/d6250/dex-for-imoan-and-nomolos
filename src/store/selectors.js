import { createSelector } from "reselect";
import { get, groupBy, reject, maxBy, minBy } from "lodash";
import { ethers } from "ethers";
import moment from "moment";

// For showing different colours for the unit price in the Buy and Sell orders,
// as well as the cases where prices go up and down
const GREEN = "#25CE8F"  // for Buy orders; for prices going up
const RED = "#F45353"    // for Sell orders; for prices going down

const account = state => get(state, "provider.account")
const tokens = state => get(state, "tokens.contracts")
const events = state => get(state, "decex.events")

const allOrders = state => get(state, "decex.allOrders.data", [])
const cancelledOrders = state => get(state, "decex.cancelledOrders.data", [])
const filledOrders = state => get(state, "decex.filledOrders.data", [])
const openOrders = state => {
    const all = allOrders(state)
    const cancelled = cancelledOrders(state)
    const filled = filledOrders(state)

    const open = reject(all, (order) => {
        const orderCancelled = cancelled.some((o) => o._ordersId.toString() === order._ordersId.toString())
        const orderFilled = filled.some((o) => o._ordersId.toString() === order._ordersId.toString())
        return(orderCancelled || orderFilled)
    })

    // console.log(all, cancelled, filled, open)  // For debug
    return open
}

export const myEventsSelector = createSelector(account, events, (account, events) => {
    events = events.filter((e) => e.args._user === account)
    return events
})

const decorateOrder = (order, tokens) => {
    let baseTokenAmount, quoteTokenAmount

    // When an order is passed into here, it only involves two tokens, base and quote,
    // either "sell (give) base, get quote", or "buy (get) base, give quote".
    // The following if-else statement distinguishes the order being which one.
    if (order._tokenGive === tokens[0].address) {
        // Sell order; sell (give) the base token, get the quote token
        baseTokenAmount = order._amountGive
        quoteTokenAmount = order._amountGet
    } else {
        // Buy order; buy (get) the base token, give the quote token
        baseTokenAmount = order._amountGet
        quoteTokenAmount = order._amountGive
    }

    // Calculate the unit price
    const precision = 100000
    let tokenUnitPrice = (quoteTokenAmount / baseTokenAmount)
    tokenUnitPrice = Math.round(tokenUnitPrice * precision) / precision

    // Add four more attributes to the order object, but not yet distinguish buy or sell order
    return({
        ...order,
        baseTokenAmount: ethers.utils.formatUnits(baseTokenAmount, "ether"),
        quoteTokenAmount: ethers.utils.formatUnits(quoteTokenAmount, "ether"),
        tokenUnitPrice,
        formattedTimestamp: moment.unix(order._timestampMade).format("h:mm:ssa d MMM D")
    })
}

const decorateOrderBookOrder = (order, tokens) => {
    const orderType = order._tokenGive === tokens[1].address ? "Buy" : "Sell"

    // Add three more attributes to the order object, specifying buy or sell order in what colour
    return({
        ...order,
        orderType,
        orderTypeClass: (orderType === "Buy" ? GREEN : RED),
        orderFillAction: (orderType === "Buy" ? "Sell" : "Buy")
    })
}

const decorateOrderBookOrders = (orders, tokens) => {

    // Make all the orders have seven more attributes by calling two different functions:
    // decorateOrder() and decorateOrderBookOrder(); now the orderType is known
    return(
        orders.map((order) => {
            order = decorateOrder(order, tokens)
            order = decorateOrderBookOrder(order, tokens)
            return(order)
        })
    )
}

export const orderBookSelector = createSelector(openOrders, tokens, (orders, tokens) => {
    // If we don't have the tokens, stop execution
    if (!tokens[0] || !tokens[1]) { return } 

    // Currently, the orders are all the orders that have ever existed on the decex, covering all markets.
    // Filter the orders according to the markets we selected, by using the filter function in JavaScript.
    // For the filter function, see documentation of: Array.prototype.filter()
    orders = orders.filter(o => o._tokenGet === tokens[0].address || o._tokenGet === tokens[1].address)
    orders = orders.filter(o => o._tokenGive === tokens[0].address || o._tokenGive === tokens[1].address)

    // Enrich all orders by adding more attributes; the orderType of each order is known
    orders = decorateOrderBookOrders(orders, tokens)
    
    // Classify all the orders by their order types, and make two arrays accordingly
    orders = groupBy(orders, "orderType")

    // Name one of the arrays as buyOrders, and sort it according to the unit price in descending order
    // For the sort function, see documentation of: Array.prototype.sort()
    const buyOrders = get(orders, "Buy", [])
    orders = {
        ...orders,
        buyOrders: buyOrders.sort((a, b) => b.tokenUnitPrice - a.tokenUnitPrice)
    }

    // Name the other array as sellOrders, and sort it according to the unit price in descending order
    // For the sort function, see documentation of: Array.prototype.sort()
    const sellOrders = get(orders, "Sell", [])
    orders = {
        ...orders,
        sellOrders: sellOrders.sort((a, b) => b.tokenUnitPrice - a.tokenUnitPrice)
    }

    // console.log(orders, buyOrders, sellOrders) // For debug: check if the orders are displayed in the console properly

    return orders
})

const buildGraphData = (orders) => {
    // Classify all the orders by their formatted timestamp, so all trades within the same day get into a candlestick
    orders = groupBy(orders, o => moment.unix(o._timestampFilled).startOf("hour").format())

    const hours = Object.keys(orders)

    const graphData = hours.map((hour) => {
        // Fetch all orders from current hour
        const group = orders[hour]
        
        // Get the open, high, low, close orders in a candlestick
        const open = group[0]
        const high = maxBy(group, "tokenUnitPrice") 
        const low = minBy(group, "tokenUnitPrice") 
        const close = group[group.length - 1]

        return({
            x: new Date(hour),
            y: [open.tokenUnitPrice, high.tokenUnitPrice, low.tokenUnitPrice, close.tokenUnitPrice]
        })
    })

    return graphData
}

export const priceChartSelector = createSelector(filledOrders, tokens, (orders, tokens) => {
    // If we don't have the tokens, stop execution
    if (!tokens[0] || !tokens[1]) { return } 

    // Currently, the orders are all the filled orders that have ever existed on the decex, covering all markets.
    // Filter the orders according to the markets we selected, by using the filter function in JavaScript.
    // For the filter function, see documentation of: Array.prototype.filter()
    orders = orders.filter(o => o._tokenGet === tokens[0].address || o._tokenGet === tokens[1].address)
    orders = orders.filter(o => o._tokenGive === tokens[0].address || o._tokenGive === tokens[1].address)

    // Sort the orders according to the timestamps in ascending order
    orders = orders.sort((a, b) => a._timestampFilled - b._timestampFilled)

    // Need the formatted timestamp as given by the decorateOrder function
    orders = orders.map(o => decorateOrder(o, tokens))

    let secondLastOrder, lastOrder
    // [secondLastOrder, lastOrder] = [orders[orders.length -2], orders[orders.length - 1]]
    [secondLastOrder, lastOrder] = orders.slice(orders.length - 2, orders.length)

    const lastPrice = get(lastOrder, "tokenUnitPrice", 0)
    const secondLastPrice = get(secondLastOrder, "tokenUnitPrice", 0)

    return ({
        secondLastPrice,
        lastPrice,
        lastPriceChange: (lastPrice - secondLastPrice > 0 ? "Up" : (lastPrice - secondLastPrice < 0 ? "Down" : "Unchanged")),
        series: [{
            data: buildGraphData(orders)
        }]
    })
})

const tokenPriceClass = (tokenUnitPrice, orderId, previousOrder) => {
    // For the first order which does not have a previous order to compare with; defaulted green
    if (previousOrder._ordersId === orderId) {
        return GREEN
    }

    // If the price rised, show in green; red otherwise
    if (tokenUnitPrice > previousOrder.tokenUnitPrice) {
        return GREEN
    } else {
        return RED
    }
}

const decorateFilledOrder = (order, previousOrder) => {
    return({
        ...order,
        tokenPriceClass: tokenPriceClass(order.tokenUnitPrice, order._ordersId, previousOrder)
    })
}

const decorateFilledOrders = (orders, tokens) => {
    let previousOrder = orders[0]

    return(
        orders.map((order) => {
            order = decorateOrder(order, tokens)
            order = decorateFilledOrder(order, previousOrder)
            previousOrder = order  // Update the previous order once it has been decorated
            return order
        })
    )
}

export const filledOrdersSelector = createSelector(filledOrders, tokens, (orders, tokens) => {
    // If we don't have the tokens, stop execution
    if (!tokens[0] || !tokens[1]) { return } 

    // Currently, the orders are all the filled orders that have ever existed on the decex, covering all markets.
    // Filter the orders according to the markets we selected, by using the filter function in JavaScript.
    // For the filter function, see documentation of: Array.prototype.filter()
    orders = orders.filter(o => o._tokenGet === tokens[0].address || o._tokenGet === tokens[1].address)
    orders = orders.filter(o => o._tokenGive === tokens[0].address || o._tokenGive === tokens[1].address)

    // Sort the orders according to the time in ascending order; for the price comparison
    orders = orders.sort((a, b) => a._timestampFilled - b._timestampFilled)

    // Put colour on the price according to whether it has rised or dropped
    orders = decorateFilledOrders(orders, tokens)

    // Sort the orders according to the time in descending order
    orders = orders.sort((a, b) => b._timestampFilled - a._timestampFilled)

    return orders
})

const decorateMyOrder = (order, tokens) => {
    const orderType = order._tokenGive === tokens[1].address ? "Buy" : "Sell"

    return({
        ...order,
        orderType,
        orderTypeClass: (orderType === "Buy" ? GREEN : RED)
    })
}

const decorateMyOrders = (orders, tokens) => {
    return(
        orders.map((order) => {
            order = decorateOrder(order, tokens)
            order = decorateMyOrder(order, tokens)
            return order
        })
    )
}

export const myOrdersSelector = createSelector(
    account,
    tokens,
    openOrders,
    (account, tokens, orders) => {
        // If we don't have the tokens, stop execution
        if (!tokens[0] || !tokens[1]) { return } 

        // Get the open orders whose user is the one in the wallet connected
        orders = orders.filter(o => o._user === account)

        // Currently, the orders are all the open orders that have ever existed on the decex, covering all markets.
        // Filter the orders according to the markets we selected, by using the filter function in JavaScript.
        // For the filter function, see documentation of: Array.prototype.filter()
        orders = orders.filter(o => o._tokenGet === tokens[0].address || o._tokenGet === tokens[1].address)
        orders = orders.filter(o => o._tokenGive === tokens[0].address || o._tokenGive === tokens[1].address)

        // Decorate the user's open orders
        orders = decorateMyOrders(orders, tokens)

        // Sort the orders according to date in descending order
        orders = orders.sort((a, b) => b._timestampMade - a._timestampMade)

        return orders
    }
)

const decorateMyTrade = (account, order, tokens) => {
    const orderCreatedByMe = (order._userMade === account)

    let orderType
    if (orderCreatedByMe) {
        orderType = order._tokenGive === tokens[1].address ? "Buy" : "Sell"
    } else {
        orderType = order._tokenGive === tokens[1].address ? "Sell" : "Buy"
    }

    return({
        ...order,
        orderType,
        orderTypeClass: (orderType === "Buy" ? GREEN : RED),
        orderTypeSign: (orderType === "Buy" ? "+" : "-")
    })
}

const decorateMyTrades = (account, orders, tokens) => {
    return(
        orders.map((order) => {
            order = decorateOrder(order, tokens)
            order = decorateMyTrade(account, order, tokens)
            return order
        })
    )
}

export const myTradesSelector = createSelector(
    account,
    tokens,
    filledOrders,
    (account, tokens, orders) => {
        // If we don't have the tokens, stop execution
        if (!tokens[0] || !tokens[1]) { return } 

        // Get the filled orders which are either created by the user and got filled, or filled by the user
        orders = orders.filter(o => o._userMade === account || o._userFilled === account)

        // Currently, the orders are all the filled orders that have ever existed on the decex, covering all markets.
        // Filter the orders according to the markets we selected, by using the filter function in JavaScript.
        // For the filter function, see documentation of: Array.prototype.filter()
        orders = orders.filter(o => o._tokenGet === tokens[0].address || o._tokenGet === tokens[1].address)
        orders = orders.filter(o => o._tokenGive === tokens[0].address || o._tokenGive === tokens[1].address)

        // Decorate the user's filled orders
        orders = decorateMyTrades(account, orders, tokens)

        // Sort the orders according to date in descending order
        orders = orders.sort((a, b) => b._timestampFilled - a._timestampFilled)

        return orders
    }
)
