const DEFAULT_PROVIDER_STATE = {}

const DEFAULT_TOKENS_STATE = {
    loaded: false,
    contracts: [],
    symbols: []
}

const DEFAULT_DECEX_STATE = {
    loaded: false,
    contract: {},
    transaction: {
        isSuccessful: false
    },
    allOrders: {
        loaded: false,
        data:[]
    }, 
    cancelledOrders: {
        data:[]
    }, 
    filledOrders: {
        data:[]
    }, 
    events: []
}

export const provider = (state = DEFAULT_PROVIDER_STATE, action) => {
    switch (action.type) {
        case "PROVIDER_LOADED":
            return {
                ...state,
                connection: action.connection
            }
        case "NETWORK_LOADED":
            return{
                ...state,
                network: action.chainId
            }
        case "ACCOUNT_LOADED":
            return{
                ...state,
                account: action.account
            }
        case "ETHER_BALANCE_LOADED":
            return{
                ...state,
                balance: action.balance
            }
        default:
            return state
    }
}

export const tokens = (state = DEFAULT_TOKENS_STATE, action) => {
    switch (action.type) {
        case "BASE_TOKEN_LOADED":
            return {
                ...state,
                loaded: true,
                contracts: [action.tokenBase], // if ...state.contracts is added, the token list in the store will go on forever
                symbols: [action.symbolBase]   // if ...state.symbols is added, the token list in the store will go on forever
            }
        case "BASE_TOKEN_WALLET_BALANCE_LOADED":
            return {
                ...state,
                balances: [action.balance]
            }
        case "QUOTE_TOKEN_LOADED":
            return {
                ...state,
                loaded: true,
                contracts: [...state.contracts, action.tokenQuote],
                symbols: [...state.symbols, action.symbolQuote]
            }
        case "QUOTE_TOKEN_WALLET_BALANCE_LOADED":
            return {
                ...state,
                balances: [...state.balances, action.balance]
            }
        default:
            return state
    }
}

export const decex = (state = DEFAULT_DECEX_STATE, action) => {
    let index, data 

    switch (action.type) {
        case "DECEX_LOADED":
            return {
                ...state,
                loaded: true,
                contract: action.decex 
            }
        case "CANCELLED_ORDERS_LOADED":
            return {
                ...state,
                cancelledOrders: {
                    loaded: true,
                    data: action.cancelledOrders
                }
            }
        case "FILLED_ORDERS_LOADED":
            return {
                ...state,
                filledOrders: {
                    loaded: true,
                    data: action.filledOrders
                }
            }
        case "ALL_ORDERS_LOADED":
            return {
                ...state,
                allOrders: {
                    loaded: true,
                    data: action.allOrders
                }
            }
        case "BASE_TOKEN_DECEX_BALANCE_LOADED":
            return {
                ...state,
                balances: [action.balance]
            }
        case "QUOTE_TOKEN_DECEX_BALANCE_LOADED":
            return {
                ...state,
                balances: [...state.balances, action.balance]
            }
        case "TRANSFER_REQUEST":
            return {
                ...state,
                transaction: {
                    transactionType: "Transfer",
                    isPending: true,
                    isSuccessful: false
                },
                transferInProgress: true
            }
        case "TRANSFER_SUCCESS":
            return {
                ...state,
                transaction: {
                    transactionType: "Transfer",
                    isPending: false,
                    isSuccessful: true 
                },
                transferInProgress: false,
                events: [action.event, ...state.events]
            }
        case "TRANSFER_FAILURE":
            return {
                ...state,
                transaction: {
                    transactionType: "Transfer",
                    isPending: false,
                    isSuccessful: false,
                    isError: true
                },
                transferInProgress: false
            }
        case "NEW_ORDER_REQUEST":
            return {
                ...state,
                transaction: {
                    transactionType: "New Order",
                    isPending: true,
                    isSuccessful: false
                }
            }
        case "NEW_ORDER_SUCCESS":
            // Prevents duplicate orders (using JavaScript function findIndex)
            // Come back here later to check how this and the if statement work; use the two console.log statements
            // (Perhaps the if statement deals with the case in which the dapp has not retrieved the events from
            // the blockchain, and the else statement deals with the case where the past events have been retrieved
            // but somehow the dapp listens for that event again.)
            index = state.allOrders.data.findIndex(order => order._ordersId.toString() === action.order._ordersId.toString())

            if (index === -1) {
                data = [...state.allOrders.data, action.order]
                // console.log(index, "1", state.allOrders.data[index]._ordersId.toString(), action.order._ordersId.toString())
            } else {
                data = state.allOrders.data
                // console.log(index, "2", state.allOrders.data, state.allOrders.data[index]._ordersId.toString(), action.order._ordersId.toString(), data)
            }

            return {
                ...state,
                allOrders: {
                    ...state.allOrders,
                    loaded: true,
                    data
                },
                transaction: {
                    transactionType: "New Order",
                    isPending: false,
                    isSuccessful: true
                },
                events: [action.event, ...state.events]
            }
        case "NEW_ORDER_FAILURE":
            return {
                ...state,
                transaction: {
                    transactionType: "New Order",
                    isPending: false,
                    isSuccessful: false,
                    isError: true
                }
            }
        case "ORDER_CANCELLATION_REQUEST":
            return {
                ...state,
                transaction: {
                    transactionType: "Order Cancellation",
                    isPending: true,
                    isSuccessful: false
                }
            }
        case "ORDER_CANCELLATION_SUCCESS":
            return {
                ...state,
                transaction: {
                    transactionType: "Order Cancellation",
                    isPending: false,
                    isSuccessful: true 
                },
                cancelledOrders: {
                    ...state.cancelledOrders,
                    data: [
                        ...state.cancelledOrders.data,
                        action.order
                    ]
                },
                events: [action.event, ...state.events]
            }
        case "ORDER_CANCELLATION_FAILURE":
            return {
                ...state,
                transaction: {
                    transactionType: "Order Cancellation",
                    isPending: false,
                    isSuccessful: false,
                    isError: true
                }
            }
        case "ORDER_FILL_REQUEST":
            return {
                ...state,
                transaction: {
                    transactionType: "Filling Order",
                    isPending: true,
                    isSuccessful: false
                }
            }
        case "ORDER_FILL_SUCCESS":
            // Prevents duplicate orders (using JavaScript function findIndex)
            // Come back here later to check how this and the if statement work; use the two console.log statements
            // (Perhaps the if statement deals with the case in which the dapp has not retrieved the events from
            // the blockchain, and the else statement deals with the case where the past events have been retrieved
            // but somehow the dapp listens for that event again.)
            index = state.filledOrders.data.findIndex(order => order._ordersId.toString() === action.order._ordersId.toString())

            if (index === -1) {
                data = [...state.filledOrders.data, action.order]
                // console.log(index, "1", state.allOrders.data[index]._ordersId.toString(), action.order._ordersId.toString())
            } else {
                data = state.filledOrders.data
                // console.log(index, "2", state.allOrders.data, state.allOrders.data[index]._ordersId.toString(), action.order._ordersId.toString(), data)
            }

            return {
                ...state,
                transaction: {
                    transactionType: "Filling Order",
                    isPending: false,
                    isSuccessful: true 
                },
                filledOrders: {
                    ...state.filledOrders,
                    data
                },
                events: [action.event, ...state.events]
            }
        case "ORDER_FILL_FAILURE":
            return {
                ...state,
                transaction: {
                    transactionType: "Filling Order",
                    isPending: false,
                    isSuccessful: false,
                    isError: true
                }
            }
        default:
            return state
    }
}
