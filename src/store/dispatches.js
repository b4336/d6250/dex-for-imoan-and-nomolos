import { ethers } from "ethers"
import TOKEN_ABI from '../abis/ERC20Token.json';
import DECEX_ABI from '../abis/ERC20DecEx.json';
// import { decex } from "./reducers";
// import { provider } from "./reducers";

// Note: Three reducers to dispatch actions to;
// 1. provider 
//    3 actions to dispatch:
//    { loadProvider, loadNetwork, loadAccountWithBalance }
// 2. tokens
//    2 actions to dispatch:
//    { loadTokens, loadBalances }
// 3. decex
//    6 actions to dispatch
//    { loadDecEx, subscribeToEvents, loadBalances, transferTokens, makeBuyOrder, makeSellOrder }

export const loadProvider = (dispatch) => {
    const connection = new ethers.providers.Web3Provider(window.ethereum)
    dispatch({ type: "PROVIDER_LOADED", connection })

    return connection
}

export const loadNetwork = async (provider, dispatch) => {
    const { chainId } = await provider.getNetwork()
    dispatch({ type: "NETWORK_LOADED", chainId })

    return chainId
}

export const loadAccountWithBalance = async (provider, dispatch) => {
    const accounts = await window.ethereum.request({ method: "eth_requestAccounts" })
    const account = ethers.utils.getAddress(accounts[0])  // ensure to get the address in right format
    dispatch({ type: "ACCOUNT_LOADED", account })

    let balance = await provider.getBalance(account)  // Ether balance
    balance = ethers.utils.formatEther(balance)  // Becomes a string
    dispatch({ type: "ETHER_BALANCE_LOADED", balance })

    return [account, balance]
}

export const loadTokens = async (provider, addresses, dispatch) => {
    const tokenBase = new ethers.Contract(addresses[0], TOKEN_ABI, provider)
    const symbolBase = await tokenBase.symbol()  // Debug note: If the smart contract is not deployed yet, this won't work
    dispatch({ type: "BASE_TOKEN_LOADED", tokenBase, symbolBase })

    const tokenQuote = new ethers.Contract(addresses[1], TOKEN_ABI, provider)
    const symbolQuote = await tokenQuote.symbol()  // Debug note: If the smart contract is not deployed yet, this won't work
    dispatch({ type: "QUOTE_TOKEN_LOADED", tokenQuote, symbolQuote })

    return [tokenBase, symbolBase, tokenQuote, symbolQuote]
}

export const loadDecEx = async (provider, address, dispatch) => {
    const decex = new ethers.Contract(address, DECEX_ABI, provider)
    dispatch({ type: "DECEX_LOADED", decex })

    return decex
}

export const subscribeToEvents = (decex, dispatch) => {
    decex.on("DepositedTokens", (token, user, amount, balance, event) => {
        dispatch({ type: "TRANSFER_SUCCESS", event })
    })
    decex.on("WithdrawnTokens", (token, user, amount, balance, event) => {
        dispatch({ type: "TRANSFER_SUCCESS", event })
    })
    decex.on("MadeOrder", (id, user, tokenGet, amountGet, tokenGive, amountGive, timestampMade, event) => {
        const order = event.args
        dispatch({ type: "NEW_ORDER_SUCCESS", order, event })
    })
    decex.on("CancelledOrder", (id, user, tokenGet, amountGet, tokenGive, amountGive, timestampMade, timestampCancelled, hoursLived, event) => {
        const order = event.args
        dispatch({ type: "ORDER_CANCELLATION_SUCCESS", order, event })
    })
    decex.on("FilledOrder", (id, userMade, userFilled, tokenGet, amountGet, tokenGive, amountGive, timestampMade, timestampFilled, hoursLived, event) => {
        const order = event.args
        dispatch({ type: "ORDER_FILL_SUCCESS", order, event })
    })
}

export const loadDecExBalances = async (decex, tokens, account, dispatch) => {
    let balance

    balance = await tokens[0].balanceOf(account)
    balance = ethers.utils.formatUnits(balance, 18)
    dispatch({ type: "BASE_TOKEN_WALLET_BALANCE_LOADED", balance })

    balance = await decex.balanceOf(tokens[0].address, account)
    balance = ethers.utils.formatUnits(balance, 18)
    dispatch({ type: "BASE_TOKEN_DECEX_BALANCE_LOADED", balance })

    balance = await tokens[1].balanceOf(account)
    balance = ethers.utils.formatUnits(balance, 18)
    dispatch({ type: "QUOTE_TOKEN_WALLET_BALANCE_LOADED", balance })

    balance = await decex.balanceOf(tokens[1].address, account)
    balance = ethers.utils.formatUnits(balance, 18)
    dispatch({ type: "QUOTE_TOKEN_DECEX_BALANCE_LOADED", balance })
}

export const loadAllOrders = async (provider, decex, dispatch) => {
    const block = await provider.getBlockNumber() // The current block

    const cancelStream = await decex.queryFilter("CancelledOrder", 0, block) // From the first block (#0) to current block
    const cancelledOrders = cancelStream.map(event => event.args)

    dispatch({ type: "CANCELLED_ORDERS_LOADED", cancelledOrders })

    const fillStream = await decex.queryFilter("FilledOrder", 0, block) // From the first block (#0) to current block
    const filledOrders = fillStream.map(event => event.args)

    dispatch({ type: "FILLED_ORDERS_LOADED", filledOrders })

    const orderStream = await decex.queryFilter("MadeOrder", 0, block) // From the first block (#0) to current block
    const allOrders = orderStream.map(event => event.args)

    dispatch({ type: "ALL_ORDERS_LOADED", allOrders })
}

export const transferTokens = async (provider, decex, transferType, token, amount, dispatch) => {
    let transaction

    dispatch({ type: "TRANSFER_REQUEST" })

    try {
        const signer = await provider.getSigner()
        const amountToTransfer = ethers.utils.parseUnits(amount.toString(), 18)

        if (transferType === "Deposit") {
            transaction = await token.connect(signer).approve(decex.address, amountToTransfer)
            await transaction.wait()
            transaction = await decex.connect(signer).depositTokens(token.address, amountToTransfer)
        } else {
            transaction = await decex.connect(signer).withdrawTokens(token.address, amountToTransfer)
        }
        await transaction.wait()

    } catch(error) {
        console.error(error)
        dispatch({ type: "TRANSFER_FAILURE" })
    }
}

export const makeBuyOrder = async (provider, decex, tokens, order, dispatch) => {
    const tokenGet = tokens[0].address
    const amountGet = ethers.utils.parseUnits(order.amount.toString(), 18)
    const tokenGive = tokens[1].address
    const amountGive = ethers.utils.parseUnits((order.amount * order.price).toString(), 18)

    dispatch({ type: "NEW_ORDER_REQUEST" })

    try {
        const signer = await provider.getSigner()
        const transaction = await decex.connect(signer).makeOrder(tokenGet, amountGet, tokenGive, amountGive)
        await transaction.wait()
    } catch(error) {
        console.error(error)
        dispatch({ type: "NEW_ORDER_FAILURE" })
    }
}

export const makeSellOrder = async (provider, decex, tokens, order, dispatch) => {
    const tokenGet = tokens[1].address
    const amountGet = ethers.utils.parseUnits((order.amount * order.price).toString(), 18)
    const tokenGive = tokens[0].address
    const amountGive = ethers.utils.parseUnits(order.amount.toString(), 18)

    dispatch({ type: "NEW_ORDER_REQUEST" })

    try {
        const signer = await provider.getSigner()
        const transaction = await decex.connect(signer).makeOrder(tokenGet, amountGet, tokenGive, amountGive)
        await transaction.wait()
    } catch(error) {
        console.error(error)
        dispatch({ type: "NEW_ORDER_FAILURE" })
    }
}

export const cancelOrder = async (provider, decex, order, dispatch) => {
    dispatch({ type: "ORDER_CANCELLATION_REQUEST" })

    try {
        const signer = await provider.getSigner()
        const transaction = await decex.connect(signer).cancelOrder(order._ordersId)
        await transaction.wait()
    } catch(error) {
        console.error(error)
        dispatch({ type: "ORDER_CANCELLATION_FAILURE" })
    }
}

export const fillOrder = async (provider, decex, order, dispatch) => {
    dispatch({ type: "ORDER_FILL_REQUEST" })

    try {
        const signer = await provider.getSigner()
        const transaction = await decex.connect(signer).fillOrder(order._ordersId)
        await transaction.wait()
    } catch(error) {
        console.error(error)
        dispatch({ type: "ORDER_FILL_FAILURE" })
    }
}
