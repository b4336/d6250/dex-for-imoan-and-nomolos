import { legacy_createStore as createStore, combineReducers, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

// Import Reducers; Analogy: The frontend database (called store) is
// composed of three partitions: 1. provider, 2. tokens and 3. decex.
import {
    provider,
    tokens,
    decex
} from "./reducers";

const reducer = combineReducers({
    provider,
    tokens,
    decex
})

const initialState = {}
const middleware = [thunk]
const store = createStore(reducer, initialState, composeWithDevTools(applyMiddleware(...middleware)))

export default store
