// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

import "hardhat/console.sol";

contract ERC20Token {

    string public name;
    string public symbol;
    uint8 public decimals;
    uint256 public totalSupply;
    mapping(address => uint256) public balanceOf;
    mapping(address => mapping(address => uint256)) public allowance;

    event Transfer(
        address indexed _from,
        address indexed _to,
        uint256 indexed _amount
    );

    event Approval(
        address indexed _owner,
        address indexed _spender,
        uint256 indexed _allowance
    );

    // Specify the name, symbol, decimals and total supply of the ERC-20 token when deployed
    constructor(
        string memory _name,
        string memory _symbol,
        uint8 _decimals,
        uint256 _totalSupply)
    {
        name = _name;
        symbol = _symbol;
        decimals = _decimals;
        totalSupply = _totalSupply * (10**decimals);
        balanceOf[msg.sender] = totalSupply;
    }

    function _transfer(address _from, address _to, uint256 _amount)
        internal
    {
        require(balanceOf[_from] >= _amount, "Reverted due to insufficient balance");
        require(_to != address(0), "Reverted due to invalid recipient's address");

        balanceOf[_from] -= _amount;
        balanceOf[_to] += _amount;

        emit Transfer(_from, _to, _amount);
    }

    function transfer(address _to, uint256 _amount)
        public
        returns(bool success)
    {
        _transfer(msg.sender, _to, _amount);
        return true;
    }

    function approve(address _spender, uint256 _allowance)
        public
        returns(bool success)
    {
        // require(balanceOf[msg.sender] >= _allowance);
        require(_spender != address(0), "Reverted due to invalid spender's address");
        allowance[msg.sender][_spender] = _allowance;
        emit Approval(msg.sender, _spender, _allowance);
        return true;
    }

    function transferFrom(address _from, address _to, uint256 _amount)
        public
        returns(bool success)
    {
        require(allowance[_from][msg.sender] >= _amount, "Reverted due to insufficient allowance");
        _transfer(_from, _to, _amount);
        allowance[_from][msg.sender] -= _amount;
        return true;
    }

}
