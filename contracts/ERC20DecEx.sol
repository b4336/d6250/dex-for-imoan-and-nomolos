// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

import "hardhat/console.sol";
import "./ERC20Token.sol";

// This decentralized ERC-20 exchange does the following:
// 0. Deposit Tokens to the Exchange
// 1. Withdraw Tokens from the Exchange
// 2. Check Balances on the Exchange
// 3. Make Orders
// 4. Cancel Orders
// 5. Fill Orders with Fees Charged

contract ERC20DecEx {

    address public feeAccount;
    uint256 public feePercent;
    mapping(address => mapping(address => uint256)) public depositedToken;
    uint256 public ordersId;

    struct _Order {
        uint256 _ordersId;            // the unique ID for each of the orders
        address _user;                // who made the order
        address _tokenGet;            // what token the order maker wishes to get
        uint256 _amountGet;           // how much of the token the order maker wishes to get
        address _tokenGive;           // what token the order maker could trade with
        uint256 _amountGive;          // how much of the token the order maker wishes to trade with
        uint256 _timestampMade;       // when the order was made
        string _status;               // either "Pending", "Cancelled" or "Filled"
        uint256 _timestampCancelled;  // if cancelled, when was it cancelled; initiated as 0
        uint256 _timestampFilled;     // if filled, when was it filled; initiated as 0
        uint256 _hoursLived;          // if cancelled or filled, how long the order had lived in hours
    }

    mapping(uint256 => _Order) public order;

    event DepositedTokens(
        address indexed _token,
        address indexed _user,
        uint256 indexed _amount,
        uint256 _balance
    );

    event WithdrawnTokens(
        address indexed _token,
        address indexed _user,
        uint256 indexed _amount,
        uint256 _balance
    );

    event MadeOrder(
        uint256 _ordersId,
        address _user,
        address _tokenGet,
        uint256 _amountGet,
        address _tokenGive,
        uint256 _amountGive,
        uint256 _timestampMade
    );

    event CancelledOrder(
        uint256 _ordersId,
        address _user,
        address _tokenGet,
        uint256 _amountGet,
        address _tokenGive,
        uint256 _amountGive,
        uint256 _timestampMade,
        uint256 _timestampCancelled,
        uint256 _hoursLived
    );

    event FilledOrder(
        uint256 _ordersId,
        address _userMade,
        address _userFilled,
        address _tokenGet,
        uint256 _amountGet,
        address _tokenGive,
        uint256 _amountGive,
        uint256 _timestampMade,
        uint256 _timestampFilled,
        uint256 _hoursLived
    );

    constructor(address _feeAccount, uint256 _feePercent) {
        feeAccount = _feeAccount;
        feePercent = _feePercent;
        ordersId = 0;
    }
    
    // 0. Deposit Tokens to the Exchange
    // Note: The user calls this function depositTokens as msg.sender;
    //       However, when whatever functions in ERC20Token(_token) is called,
    //       that function's caller becomes this smart contract of the dex.
    //       Therefore, transferFrom has to be used instead of transfer;
    //       the caller calling transferFrom is this dex's smart contract
    //       instead of msg.sender, who calls depositTokens in the first place.
    //       This is also why the function approve cannot be called here;
    //       this dex's smart contract cannot call it as only msg.sender can call it.
    function depositTokens(address _token, uint256 _amount) public {
        // ERC20Token(_token).approve(address(this), _amount); << This cannot be called
        // require(ERC20Token(_token).transfer(address(this), _amount)); << This cannot be called

        require(ERC20Token(_token).transferFrom(msg.sender, address(this), _amount),
            "Reverted due to insufficient allowance or balance");
        depositedToken[_token][msg.sender] += _amount;
        emit DepositedTokens(_token, msg.sender, _amount, depositedToken[_token][msg.sender]);
    }

    // 1. Withdraw Tokens from the Exchange
    function withdrawTokens(address _token, uint256 _amount) public {
        require(ERC20Token(_token).transfer(msg.sender, _amount),
            "Reverted due to insufficient balance to make the said withdrawal");
        depositedToken[_token][msg.sender] -= _amount;
        emit WithdrawnTokens(_token, msg.sender, _amount, depositedToken[_token][msg.sender]);
    }

    // 2. Check Balances on the Exchange
    function balanceOf(address _token, address _user) public view returns(uint256) {
        return depositedToken[_token][_user];
    }

    // 3. Make Orders
    function makeOrder(address _tokenGet, uint256 _amountGet, address _tokenGive, uint256 _amountGive) public {
        require(balanceOf(_tokenGive, msg.sender) >= _amountGive,
            "Reverted due to insufficient balance to make the said order");
        ordersId++;
        order[ordersId] = _Order(ordersId, msg.sender, _tokenGet, _amountGet, _tokenGive, _amountGive, block.timestamp, "Pending", 0, 0, 0);
        emit MadeOrder(ordersId, msg.sender, _tokenGet, _amountGet, _tokenGive, _amountGive, order[ordersId]._timestampMade);
    }

    // 4. Cancel Orders
    function cancelOrder(uint256 _ordersId) public {
        _Order storage _order = order[_ordersId];

        require(_order._user == msg.sender,
            "Reverted since the canceller does not own the order");
        require(keccak256(abi.encodePacked(_order._status)) == keccak256("Pending"),
            "Reverted since the order is not pending"); // Cannot compare two strings directly using == operator

        _order._status = "Cancelled";
        _order._timestampCancelled = block.timestamp;
        _order._hoursLived = (_order._timestampCancelled - _order._timestampMade) / (60*60);

        emit CancelledOrder(_order._ordersId, _order._user, _order._tokenGet, _order._amountGet, _order._tokenGive, _order._amountGive, _order._timestampMade, _order._timestampCancelled, _order._hoursLived);
    }

    // 5. Fill Orders with Fees Charged
    function _swapWithFee(address _maker, address _filler, address _tokenGet, uint256 _amountGet, address _tokenGive, uint256 _amountGive) internal {
        // The order maker gives and gets the tokens
        depositedToken[_tokenGive][_maker] -= _amountGive;
        depositedToken[_tokenGet][_maker] += _amountGet;

        // The order filler gets and gives the tokens as well as pays the fee
        // Solidity cannot handle _amountGet * (feePercent/100);
        // So have to express as: (_amountGet * feePercent)/100
        depositedToken[_tokenGive][_filler] += _amountGive;
        depositedToken[_tokenGet][_filler] -= _amountGet + ((_amountGet * feePercent)/100);

        // The exchange gets the fee paid by the filler
        ERC20Token(_tokenGet).transfer(feeAccount, (_amountGet * feePercent)/100);
    }

    function fillOrder(uint256 _ordersId) public {
        _Order storage _order = order[_ordersId];
        
        require(_order._ordersId == _ordersId,
            "Reverted since this requested order does not exist");
        require(keccak256(abi.encodePacked(_order._status)) == keccak256("Pending"),
            "Reverted since the order is not pending");
        require(msg.sender != _order._user,
            "Reverted since the order maker himself/herself tries to fill the order");
        require(balanceOf(_order._tokenGet, msg.sender) >= _order._amountGet + _order._amountGet * (feePercent/100),
            "Reverted since the balance is insufficient for filling the order plus paying the fee");

        _swapWithFee(_order._user, msg.sender, _order._tokenGet, _order._amountGet, _order._tokenGive, _order._amountGive);
        _order._status = "Filled";
        _order._timestampFilled = block.timestamp;
        _order._hoursLived = (_order._timestampFilled - _order._timestampMade) / (60*60);

        emit FilledOrder(_order._ordersId, _order._user, msg.sender, _order._tokenGet, _order._amountGet, _order._tokenGive, _order._amountGive, _order._timestampMade, _order._timestampFilled, _order._hoursLived); 
    }

}
